import sbt._
import Keys._
import Tests._

/**
 * This is a simple sbt setup generating Slick code from the given
 * database before compiling the projects code.
 */
object myBuild extends Build {

  // code generation task
  lazy val slick = TaskKey[Seq[File]]("gen-tables")
  lazy val slickCodeGenTask = (baseDirectory in Compile, dependencyClasspath in Compile, runner in Compile, streams) map { (dir, cp, r, s) =>
    val outputDir = (dir / "app").getPath // place generated files in sbt's managed sources folder
    val url = "jdbc:mysql://localhost:3306/piscisweb"
    val jdbcDriver = "com.mysql.jdbc.Driver"
    val slickDriver = "scala.slick.driver.MySQLDriver"
    val pkg = "models"
    val user = "root"
    val password = "f4llAngel$369";
    toError(r.run("codegen.CustomSourceCodeGenerator", cp.files, Array(slickDriver, jdbcDriver, url, outputDir, pkg, user, password), s.log))
    val fname = outputDir + "/Tables.scala"
    Seq(file(fname))
  }
}