name := """PiscisWeb"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala).
				settings(
					slick <<= slickCodeGenTask,
					scalacOptions ++= Seq("-feature")
				).dependsOn(codegen)
				
lazy val codegen = project.settings(
	scalaVersion := "2.11.2",
	libraryDependencies ++= List(
        "com.typesafe.slick" %% "slick-codegen" % "2.1.0",
        "org.scala-lang" % "scala-reflect" % "2.11.2"
      ))

scalaVersion := "2.11.2"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "com.typesafe.play" %% "play-slick" % "0.8.0",
  "com.typesafe.slick" %% "slick" % "2.1.0",
  "com.typesafe.slick" %% "slick-codegen" % "2.1.0",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "mysql" % "mysql-connector-java" % "5.1.31",
  "org.webjars" %% "webjars-play" % "2.3.0",
  "org.webjars" % "bootstrap" % "3.3.0",
  "org.webjars" % "angularjs" % "1.3.1",
  "org.webjars" % "requirejs" % "2.1.14-3",
  "org.webjars" % "jquery" % "2.1.1",
  "org.mindrot" % "jbcrypt" % "0.3m",
  "org.webjars" % "angular-moment" % "0.8.2-1",
  "org.webjars" % "momentjs" % "2.8.3",
  "org.webjars" % "angular-ui-bootstrap" % "0.12.0",
  "org.webjars" % "angular-ui-router" % "0.2.12",
  "org.webjars" % "angular-dateparser" % "1.0.9",
  "org.webjars" % "angular-ui-date" % "0.0.5",
  "org.webjars" % "angular-growl-2" % "0.6.0",
  "org.webjars" % "mobile-angular-ui" % "1.1.0-beta.29-2",
  "org.webjars" % "jquery-ui" % "1.11.2",
  "org.webjars" % "notifyjs" % "0.3.2",
  "org.seleniumhq.selenium" % "selenium-java" % "2.43.0",
  "org.scalaz" %% "scalaz-core" % "7.0.6"
)

pipelineStages := Seq(digest)
