package models

import play.api.test._
import play.api.test.Helpers._
import controllers.FuncHelpers._

class ModulosServiceSpec extends PlaySpecification {
  "ModulosService#getProcesosxModeloUser" should {

    val modulo = "Configuraciones"

    "Retornar secuencia vacia si el modulo es invalido" in new WithDbData with PermisosProcesosData {
      runSession { implicit s =>
        val t = for {
          admin <- loginService.getUser("admin")
          mod = modulosService.getProcesosxModuloUser(admin.id.get, "invalid")
        } yield {
          mod must be empty
        }
        t.getOrElse(failure)
      }
    }

    "Retornar vacio si el usuario no es valido" in new WithDbData with PermisosProcesosData {
      runSession { implicit s =>
        modulosService.getProcesosxModuloUser(98, modulo) must be empty
      }
    }

    "Retornar secuencia vacia si el usuario no tiene permisos a ningun proceso del modulo dado" in new WithDbData with PermisosProcesosData {

      runSession { implicit s =>
        val t = for {
          user <- loginService.getUser("user")
          mod = modulosService.getProcesosxModuloUser(user.id.get, "invalid")
        } yield {
          mod must be empty
        }
        t.getOrElse(failure)
      }
    }

    "Admin tiene permiso de ingresar, actualizar, consultar, eliminar e insertar al proceso Usuarios" in new WithDbData with PermisosProcesosData {
      runSession { implicit s =>
        val t = for {
          admin <- loginService.getUser("admin")
          mod = modulosService.getProcesosxModuloUser(admin.id.get, modulo)
          map = pairsToMapSet(mod)
          (procUsuarios, permisos) <- map.find { case (p, s) => p.nombre.toLowerCase == "usuarios" }
        } yield {
          permisos.map(_.nombre.toLowerCase) must contain("ingresar", "actualizar", "eliminar", "consultar", "insertar")
        }
        t.getOrElse(failure)
      }
    }

    "Admin tiene permiso de ingresar, actualizar, consultar, eliminar e insertar al proceso modulos" in new WithDbData with PermisosProcesosData {
      runSession { implicit s =>
        val t = for {
          admin <- loginService.getUser("admin")
          mod = modulosService.getProcesosxModuloUser(admin.id.get, modulo)
          map = pairsToMapSet(mod)
          (procUsuarios, permisos) <- map.find { case (p, s) => p.nombre.toLowerCase == "modulos" }
        } yield {
          permisos.map(_.nombre.toLowerCase) must contain("ingresar", "actualizar", "eliminar", "consultar", "insertar")
        }
        t.getOrElse(failure)
      }
    }

    "Admin tiene permiso de ingresar, actualizar, consultar, eliminar e insertar al proceso rolesxusuario" in new WithDbData with PermisosProcesosData {
      runSession { implicit s =>
        val t = for {
          admin <- loginService.getUser("admin")
          mod = modulosService.getProcesosxModuloUser(admin.id.get, modulo)
          map = pairsToMapSet(mod)
          (procUsuarios, permisos) <- map.find { case (p, s) => p.nombre.toLowerCase == "rolesxusuario" }
        } yield {
          permisos.map(_.nombre.toLowerCase) must contain("ingresar", "actualizar", "eliminar", "consultar", "insertar")
        }
        t.getOrElse(failure)
      }
    }
  }
}