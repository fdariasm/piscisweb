package models

import scala.slick.driver.MySQLDriver.simple.Database
import scala.slick.driver.MySQLDriver.simple.Session
import scala.slick.driver.MySQLDriver.simple.intColumnType
import scala.util.Failure
import scala.util.Success

import org.specs2.execute.AsResult
import org.specs2.execute.Result

import models.repositories.LoginRepo
import models.repositories.ModulosRepo
import models.repositories.PermisosRepo
import models.services.LoginServiceImp
import models.services.ModulosServiceImp
import play.api.db.DB
import play.api.test.FakeApplication
import play.api.test.WithApplication




class WithDbData(app: FakeApplication = FakeApplication()) extends WithApplication(app) {
  def ds = DB.getDataSource("test")
  def slickDb = Database.forDataSource(ds)
  override def around[T: AsResult](t: => T): Result = super.around {
    setup
    val result = AsResult.effectively(t)
    teardown
    result
  }
  def setup = {
    slickDb.withSession { implicit session =>
      //      ddl.create
    }
  }

  def teardown = {
    slickDb.withSession { implicit session =>
    }
  }

  def runSession[A](f: Session => A) = slickDb.withSession { implicit session =>
    f(session)
  }
}

trait PermisosProcesosData{ this: WithDbData =>
  import models.TestData
  runSession{ implicit s =>
    TestData.insertTestData match{
      case Success(_) => 
      case Failure(exception) => throw exception
    }
  }
  val modulosService = new ModulosServiceImp( new ModulosRepo {}, new PermisosRepo{} )
  val loginService = new LoginServiceImp( new LoginRepo {}, new PermisosRepo {} )
}