package helper

object TestHelpers {

  def testingDatabase(name: String = "default"): Map[String, String] = {
    Map(
      ("db." + name + ".driver") -> "com.mysql.jdbc.Driver",
      ("db." + name + ".url") -> "jdbc:mysql://localhost:3306/piscisweb_test",
      ("db." + name + ".user") -> "root",
      ("db." + name + ".password") -> "f4llAngel$369")
  }

}