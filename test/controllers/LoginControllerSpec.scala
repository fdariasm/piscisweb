package controllers

import java.util.Date
import org.specs2.mock.Mockito
import controllers.control.Secured
import models.Tables.UsuariosRow
import models.services.LoginService
import play.api.mvc.Controller
import play.api.mvc.RequestHeader
import play.api.mvc.Result
import play.api.mvc.Session
import play.api.test.PlaySpecification
import models.Tables._
import play.api.mvc.Results
import scala.slick.driver.MySQLDriver.simple._
import controllers.control.secutils._

class LoginControllerSpec extends PlaySpecification with Mockito {
  val loginServiceMock = mock[LoginService]


  object loginController extends  LoginController with Controller with Secured with TestingSecured {
    val loginService: LoginService = loginServiceMock
  }

  val username = "admin"
  val password = "qwerty"
  val loginInfo = List(("username", username), ("password", password))

}