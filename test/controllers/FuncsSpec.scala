package controllers

import org.specs2._
import FuncHelpers._
 
class FuncsSpec extends Specification{
  def is = s2"""
    Specification for pairsToMapSet
    
    pairsToMapSet should pass following tests
    test 1  $test1
    test 2  $test2
    test 3  $test3
    
    """

  def test1 = {
    val t1 = Seq((1, "a"), (1, "b"), (2, "b"), (3, "a"), (4, "a"), (4, "c"))
    val re1 = Map(1 -> Set("a", "b"), 2 -> Set("b"), 3-> Set("a"), 4 -> Set("a", "c"))
    pairsToMapSet( t1 ) must_== re1
  }
  
  case class Ta(i: Int)
  
  def test2 = {
    val t1 = Seq((1, Ta(1)), (1, Ta(4)), (2, Ta(7)), (3, Ta(1)), (4, Ta(4)), (4, Ta(3)))
    val re1 = Map(1 -> Set(Ta(1), Ta(4)), 2 -> Set(Ta(7)), 3-> Set(Ta(1)), 4 -> Set(Ta(4), Ta(3)))
    pairsToMapSet( t1 ) must_== re1
  }
  
  case class Key(s: String)
  
  def test3 = {
    val t1 = Seq((Key("one"), Ta(1)), (Key("one"), Ta(4)), (Key("two"), Ta(7)), (Key("three"), Ta(1)), (Key("four"), Ta(4)), (Key("four"), Ta(3)))
    val re1 = Map(Key("one") -> Set(Ta(1), Ta(4)), Key("two") -> Set(Ta(7)), Key("three")-> Set(Ta(1)), Key("four") -> Set(Ta(4), Ta(3)))
    pairsToMapSet( t1 ) must_== re1
  }
}