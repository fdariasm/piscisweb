package specs

import specs.BaseSpec

trait AppActions { this: BaseSpec =>

  def login(u: String, p: String) = {
    browser.goTo("/login")
    browser.$("#username").text(u)
    browser.$("#password").text(p)
    browser.$("#submit").click()
  }
}