package specs

import specs.BaseSpec
import play.api.db.DB
import specs.SlickDB
import models.Tables._
import scala.slick.driver.MySQLDriver.simple._

class ProcLoginSpec extends BaseSpec with SlickDB {
  def setupDb: Unit = {
    println("Setting up DB")
    slickDb.withSession { implicit session =>
      SlickDB.cleanTables
    }
  }
  def dbCleanup: Unit = println("Tearing down DB")

  override def is = sequential ^
    s2"""
      
    This is a specification to check the 'Hello world' string

    The 'Hello world' string should
      contain 11 characters                             $e1
      start with 'Hello'                                $e2
      end with 'world'                                  $e3
                                                        """

  def e1 = "Hello world" must have size (11)
  def e2 = "Hello world" must startWith("Hello")
  def e3 = "Hello world" must endWith("world")
}