package specs

package specs
import org.specs2._
import org.specs2.specification.Around
import org.specs2.execute.AsResult
import org.specs2.specification.BeforeExample
import org.specs2.specification.AfterExample
import org.specs2.specification.Step
import play.api.test._
import helper.TestHelpers._
import play.api.test.PlayRunners
import play.api.Play
import play.api.test.PlayRunners
import org.specs2.specification.Fragments

trait BaseSpec extends PlaySpecification {
  implicit def app = FakeApplication(additionalConfiguration = testingDatabase())
  def webDriver = WebDriverFactory(FIREFOX)
  def port: Int = Helpers.testServerPort
  def setupDb: Unit
  def dbCleanup: Unit

  lazy val browser: TestBrowser = TestBrowser(webDriver, Some("http://localhost:" + port))
  private lazy val testServer = TestServer(port, app)

  def setup = {
    //    println("####Starting the play application server####")
    testServer.start()
    browser.maximizeWindow();
    setupDb
  }

  def cleanup = {
    //    println("####Stoping the play application server####")
    testServer.stop()
    browser.quit()
    dbCleanup
  }

  override def map(fs: => Fragments) = Step(setup) ^ fs ^ Step(cleanup)
}

trait SlickDB { this: BaseSpec =>
  import play.api.db.DB
  import scala.slick.driver.JdbcDriver.simple._
  def ds = DB.getDataSource()
  def slickDb = Database.forDataSource(ds)
}

object SlickDB {
  import models.Tables._
  import play.api.db.DB
  import scala.slick.driver.MySQLDriver.simple._
  import org.mindrot.jbcrypt.BCrypt

  def cleanTables(implicit s: Session) = {
    
  }

  def insertUser(user: String, pass: String, email: String, auth: String, name: String)(implicit s: Session) {
    val password = BCrypt.hashpw(pass, BCrypt.gensalt())

//    Users.map(u => (u.username, u.password, u.email, u.authority, u.name))
//      .insert((user, password, email, auth, name)).run
    
  }
}