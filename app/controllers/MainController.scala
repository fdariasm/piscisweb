package controllers

import play.api._
import play.api.mvc._
import controllers.control.Secured
import models.services.LoginService
import scala.slick.driver.MySQLDriver.simple._
import models.repositories.PermisosRepo
import models.repositories.LoginRepo
import models.services.LoginServiceImp
import play.mvc.Security.Authenticated
import controllers.control.Modulos

import models.Tables._

import play.api.libs.json._
import play.api.libs.json.JsValue

trait MainController { this: Controller with Secured with Modulos =>
  import controllers.helpers.FuncHelpers._

  def vistaPrinc(instancia: String) = (authenticated(instancia) andThen WithModules) { implicit rs =>
    Ok(views.html.indexMain()(Some(rs)))
  }

  case class PermisoProceso(proceso: String, permiso: Boolean)
  implicit val listMap = Json.writes[PermisoProceso]

  def tienePermiso = Authenticated(parse.json) { rs =>
    val procesosJson = rs.body.validate[List[String]]
    val tipos = Seq("vista")
    val r = for {
      iduser <- optionToEither(rs.user.id, Json.obj("status" -> "ERROR", "message" -> "iduser missing")).right
      procesos <- jsonToEither(procesosJson)(jsonValidateError(_)).right
    } yield {
      Json.toJson(procesos.map(proceso => PermisoProceso(proceso, loginService.tienePermiso(proceso, tipos, iduser)(rs.dbsession))))
    }
    r.fold(BadRequest(_), Ok(_))
  }

  def procesosUsuario( modulo: String ) = Authenticated{ implicit rs =>
    val permisos = modulosService.getProcesosxModuloUser(rs.user.id.get, modulo)
    val strPermisos = pairsToMapSet(permisos).map { case (p, s) => (p.nombre, s.map(_.nombre)) }
    Ok(Json.toJson( strPermisos ))
  }

  def apiTest = Authenticated { implicit rs =>
    val permisos = modulosService.getProcesosxModuloUser(rs.user.id.get, "Configuraciones")

    val tras = pairsToMapSet(permisos).map { case (p, s) => (p.nombre, s.map(_.nombre)) }

    Ok(Json.toJson(tras))
  }

}

object MainController extends Controller with MainController with Secured with ValidUser with Modulos with ProdDep {
}

