package controllers

import play.api._
import play.api.mvc._
import controllers.control.Secured
import controllers.control.Registro
import models.services.LoginService
import scala.slick.driver.MySQLDriver.simple._
import models.repositories.RolesRepo
import models.repositories.LoginRepo
import models.services.LoginServiceImp
import models.services.RolesService
import models.services.RolesServiceImp
import models.Tables._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import models.repositories.ParametrosRepo
import models.repositories.PermisosRepo
import models.services.ParametrosService
import models.services.ParametrosServiceImp
import play.api.i18n.Messages

trait RolesController extends CrudController[Int, RolesRow] { this: Controller with Secured with Registro =>

  import controllers.helpers.FuncHelpers._

  implicit val modelReads = Json.reads[RolesRow]
  implicit val modelWrites = Json.writes[RolesRow]

  def rolesService: RolesService

  def modelService = rolesService

  def msgSaved(model: RolesRow): String = s"Rol guardado"
  def msgUpdated(model: RolesRow): String = s"Rol ${model.id} actualizado"
  def msgDeleted(model: RolesRow): String = s"Rol ${model.id} eliminado"
  def msgNotFound(id: Int) = Messages("rol.invalido")

  val proceso = "roles"

  def addRol = insertAction(proceso)(parse.json) {
    ApiResponse(_) { create }
  }

  def updateRol(id: Int) = updateAction(proceso)(parse.json) {
    ApiResponse(_) { update(id) }
  }

  def deleteRol(id: Int) = deleteAction(proceso) {
    ApiResponse(_) { delete(id) }
  }

  def getRol(id: Int) = queryAction(proceso) {
    ApiResponse(_) { get(id) }
  }

  def getRoles= accessAction(proceso) {
    ApiResponse(_) { all }
  }

}

object RolesController extends Controller with RolesController with Secured with ValidUser with SecuredDep with Registro{
  val rolesService: RolesService = new RolesServiceImp(new RolesRepo{})
}