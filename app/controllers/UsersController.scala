package controllers

import play.api._
import play.api.mvc._
import controllers.control.Secured
import models.services.LoginService
import scala.slick.driver.MySQLDriver.simple._
import models.repositories.PermisosRepo
import models.repositories.LoginRepo
import models.services.LoginServiceImp
import models.services.ParametrosService
import models.repositories.ParametrosRepo
import models.services.ParametrosServiceImp
import models.Tables._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import java.sql.Timestamp
import play.api.i18n.Messages
import java.text.SimpleDateFormat
import java.util.Date
import controllers.control.Registro

trait UsersController extends CrudController[Int, UsuariosRow] { this: Controller with Secured with Registro =>
  import controllers.helpers.FuncHelpers._
  
  implicit val wrs: Writes[Timestamp] = new Writes[Timestamp]{
    def writes(t: Timestamp) = {
      JsString(dateFormater.format(t.getTime()))
    }
  }

  implicit val modelReads = (
    (__ \ 'id).readNullable[Int] and
    (__ \ 'identificacion).read[String](maxLength[String](20)) and
    (__ \ 'usuario).read[String](maxLength[String](25)) and
    (__ \ 'nombre).read[String](maxLength[String](100)) and
    (__ \ 'email).read[String](email) and
    (__ \ 'fecharetiro).readNullable[String] and
    (__ \ 'reiniciarclave1).read[Boolean] and
    (__ \ 'reiniciarclave2).read[Boolean])((id, identificacion, usuario, nombre, email, fecharetiro, rc1, rc2) => {
      val claveDef = identificacion
      if (id.isEmpty) //nuevo usuario
        UsuariosRow(identificacion, usuario, nombre, email, claveDef, claveDef, fecharetiro.map(getTimestamp(_)), 
            true, true)
      else
        UsuariosRow(identificacion, usuario, nombre, email,
          "", "", fecharetiro.map(getTimestamp(_)), rc1, rc2, id)
    })

  //Json.writes[UsuariosRow]
  implicit val modelWrites = (
    (__ \ 'id).writeNullable[Int] and
    (__ \ 'identificacion).write[String] and
    (__ \ 'usuario).write[String] and
    (__ \ 'nombre).write[String] and
    (__ \ 'email).write[String] and
    (__ \ 'fecharetiro).writeNullable[Timestamp] and
    (__ \ 'reiniciarclave1).write[Boolean] and
    (__ \ 'reiniciarclave2).write[Boolean])((u: UsuariosRow) =>
      (u.id, u.identificacion, u.usuario, u.nombre, u.email,
        u.fecharetiro, u.reiniciarclave1, u.reiniciarclave2))
        
  def modelService = loginService

  def msgSaved(model: UsuariosRow): String = Messages("usuario.creado")
  def msgUpdated(model: UsuariosRow): String = Messages("usuario.actualizado").format(model.id)
  def msgDeleted(model: UsuariosRow): String = Messages("usuario.eliminado").format(model.id)
  def msgNotFound(id: Int) = Messages("usuario.invalido")

  
  val proceso = "usuarios"

  lazy val permConsultar = authAction(proceso, "consultar") //optimizacion prematura

  def addUser = insertAction(proceso)(parse.json) {
    ApiResponse(_) { create }
  }

  def updateUser(id: Int) = updateAction(proceso)(parse.json) {
    ApiResponse(_) { update(id) }
  }

  def deleteUser(id: Int) = deleteAction(proceso) {
    ApiResponse(_) { delete(id) }
  }

  def getUser(id: Int) = queryAction(proceso) {
    ApiResponse(_) { get(id) }
  }

  def getUsers = accessAction(proceso) {
    ApiResponse(_) { all }
  }
  
//  def rolesUsuario = authAction("rolporusuario", "ingresar"){ implicit rs =>
//    
//    Ok
//  }
}

object UsersController extends Controller with UsersController with Secured with ValidUser with SecuredDep with Registro{
}