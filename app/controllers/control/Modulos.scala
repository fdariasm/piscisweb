package controllers.control

import play.api.mvc._
import scala.concurrent.Future
import controllers.routes
import models.services.ModulosService
import scala.language.postfixOps
import scala.language.higherKinds

trait Modulos { this: Controller with Secured =>

  def modules(datasource: String): Seq[Modulo] = List(
    Modulo(1, "Principal", routes.MainController.vistaPrinc(datasource)),
    Modulo(2, "Configuraciones", routes.ConfigController.configView(datasource)))

  def modulosService: ModulosService

  object WithModules extends ActionRefiner[AuthenticatedDBRequest, WithModulesRequest] {
    def refine[A](rs: AuthenticatedDBRequest[A]) = Future.successful {
      val m = modules(rs.datasource).map { x => (x.nombre.toLowerCase, x) } toMap
      val modulos = modulosService.modulosByUser(rs.user.id.get)(rs.dbsession).map { x => m.get(x.nombre.toLowerCase) }

      Right(
        WithModulesRequest(
          modulos.filter(_.isDefined).map(_.get).sortBy(_.orden),
          rs))
    }
  }

  type Module = String

  def canAccessModule(module: Module) = new CustomActionFilter[AuthenticatedDBRequest] {
    def filter[A](input: AuthenticatedDBRequest[A]) = {
      for{
        iduser <- input.user.id
        if !modulosService.canAccess(iduser, module)(input.dbsession)
      } yield onInvalidCredentials(input)
    }
  }
}

trait CustomActionRefiner[-R[_], +P[_]] extends ActionFunction[R, P] {

  protected def refine[A](request: R[A]): Either[Result, P[A]]

  final def invokeBlock[A](request: R[A], block: P[A] => Future[Result]) =
    refine(request).fold(Future.successful _, block(_))
}

trait CustomActionFilter[R[_]] extends CustomActionRefiner[R, R] {
  protected def filter[A](request: R[A]): Option[Result]

  final protected def refine[A](request: R[A]) =
    filter(request).toLeft(request)
}