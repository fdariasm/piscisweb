package controllers.control

import play.api.mvc._
import models.Tables._
import play.api.libs.iteratee._
import play.api.mvc.Results._
import play.api._
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import models.services.LoginService
import scala.slick.driver.MySQLDriver.simple.Session
import scala.slick.driver.MySQLDriver.simple.Database
import play.api.Play.current
import play.api.db.DB
import play.api.i18n.Messages

trait SecResult { this: Controller =>

  def username(request: RequestHeader)(implicit s: Session): Option[UsuariosRow]

  def onUnauthorized(request: RequestHeader): Result = Unauthorized(Messages("login.nosesion"))

  def onInvalidCredentials(request: RequestHeader) = Forbidden("Access denied")

  def loginService: LoginService

  def authenticated[A](rs: Request[A])(f: AuthenticatedDBRequest[A] => Result): Result = {
    Database.forDataSource(DB.getDataSource()).withTransaction { implicit s =>
      username(rs).map { user =>
        f(AuthenticatedDBRequest(user, s, rs))
      }.getOrElse {
        onUnauthorized(rs)
      }
    }
  }

  def authenticatedAsync[A](rs: Request[A])(f: AuthenticatedDBRequest[A] => Future[Result]): Future[Result] = {
    Database.forDataSource(DB.getDataSource()).withTransaction { implicit s =>
      username(rs).map { user =>
        f(AuthenticatedDBRequest(user, s, rs))
      }.getOrElse {
        Future.successful(onUnauthorized(rs))
      }
    }
  }

  def adminCheck[A](rs: AuthenticatedDBRequest[A])(f: AuthenticatedDBRequest[A] => Result): Result = {
    val roles = loginService.getRoles(rs.user)(rs.dbsession)
    if (roles.map(_.nombre).contains("admin"))
      f(rs)
    else
      Forbidden
  }


  def tienePermiso[A](proceso: String, tipos: Seq[String])(rs: AuthenticatedDBRequest[A])(f: AuthenticatedDBRequest[A] => Result): Result = {
    val res = for {
      idusuario <- rs.user.id
      if loginService.tienePermiso(proceso, tipos, idusuario)(rs.dbsession)
    } yield {
      f(rs)
    }
    res.getOrElse((onInvalidCredentials(rs)))
  }

  def tienePermisoAsync[A](proceso: String, tipos: Seq[String])(rs: AuthenticatedDBRequest[A])(f: AuthenticatedDBRequest[A] => Future[Result]): Future[Result] = {
    val res = for {
      idusuario <- rs.user.id
      if loginService.tienePermiso(proceso, tipos, idusuario)(rs.dbsession)
    } yield {
      f(rs)
    }
    res.getOrElse(Future.successful(onInvalidCredentials(rs)))
  }

  //  def test = Action {
  //    authenticated(_) { ars =>
  //      //      adminCheck(ars) { _ =>
  //      logging(ars) { _ =>
  //        LogResult(Structure("Esto se muestra en consola?"), Ok("esta logueado"))
  //      }
  //      //      }
  //    }
  //  }
  //
  //  def test2 = Action(parse.json) {
  //    authenticated(_) {
  //      adminCheck(_) {
  //        logging(_) { rs =>
  //          LogResult(Structure("Esto se muestra en consola?"), Ok("esta logueado"))
  //        }
  //      }
  //    }
  //  }

}