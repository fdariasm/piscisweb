package controllers.control

import play.api.mvc._
import models.Tables.UsuariosRow
import scala.slick.driver.MySQLDriver.simple.Session


case class AuthenticatedDBRequest[A](
  val user: UsuariosRow,
  val dbsession: Session,
  request: Request[A],
  datasource: String = "default") extends WrappedRequest[A](request)
  
abstract class AccesosRequest[A](
  val proceso: String,
  val request: AuthenticatedDBRequest[A]) extends WrappedRequest[A](request){
  def user = request.user
  def dbsession = request.dbsession
  def datasource = request.datasource
}
  
case class PermisosRequest[A](
  val proceso: String,
  val permisos: Seq[String],
  request: AuthenticatedDBRequest[A]) extends WrappedRequest[A](request){
  def user = request.user
  def dbsession = request.dbsession
  def datasource = request.datasource
}

case class InsercionRequest[A](
  override val proceso: String,
  override val request: AuthenticatedDBRequest[A]) extends AccesosRequest[A](proceso, request){
}

case class IngresoRequest[A](
  override val proceso: String,
  override val request: AuthenticatedDBRequest[A]) extends AccesosRequest[A](proceso, request){
}

case class ActualizacionRequest[A](
  override val proceso: String,
  override val request: AuthenticatedDBRequest[A]) extends AccesosRequest[A](proceso, request){
}

case class ConsultaRequest[A](
  override val proceso: String,
  override val request: AuthenticatedDBRequest[A]) extends AccesosRequest[A](proceso, request){
}

case class EliminacionRequest[A](
  override val proceso: String,
  override val request: AuthenticatedDBRequest[A]) extends AccesosRequest[A](proceso, request){
}

case class Modulo(orden: Int, nombre: String, ruta: play.mvc.Call)

case class WithModulesRequest[A](
  val mod: Seq[Modulo],
  val request: AuthenticatedDBRequest[A]) extends WrappedRequest[A](request) {
  def user = request.user
  def dbsession = request.dbsession
  def datasource = request.datasource 
}