package controllers.control

import play.api.mvc._
import models.Tables._
import play.api.libs.iteratee._
import play.api.mvc.Results._
import play.api._
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import models.services.LoginService
import scala.slick.driver.MySQLDriver.simple.Session
import scala.slick.driver.MySQLDriver.simple.Database
import scala.util.Try
import play.api.Play.current
import play.api.db.DB
import controllers.routes
import play.api.i18n.Messages

trait Secured { this: Controller =>

  def dsKey: String

  def username(request: RequestHeader)(implicit s: Session): Option[UsuariosRow]

  def getDs(request: RequestHeader): Option[String] = request.session.get(dsKey)

  def onUnauthorized(request: RequestHeader): Result = Unauthorized(Messages("login.nosesion"))

  def onUnauthorized(instancia: String): Result = Results.Redirect(routes.LoginController.login(instancia))

  def onInvalidCredentials(request: RequestHeader) = Forbidden(Messages("login.denegado"))

  def loginService: LoginService

  type Proceso = String

  object Authenticated extends ActionBuilder[AuthenticatedDBRequest] {

    def invokeBlock[A](request: Request[A], block: (AuthenticatedDBRequest[A]) => Future[Result]) =
      authenticate(request, block)

    def authenticate[A](request: Request[A], block: (AuthenticatedDBRequest[A]) => Future[Result]) = {
      val instancia = getDs(request)
      val tryds = instancia.flatMap(name => Try(DB.getDataSource(name)).toOption.map((_, name)))
      tryds.flatMap {
        case (ds, dsname) =>
          Database.forDataSource(ds).withTransaction { implicit s =>
            username(request).map { user =>
              block(new AuthenticatedDBRequest(user, s, request, dsname))
            }
          }
      } getOrElse {
        Future.successful(onUnauthorized(request))
      }
    }
  }

  def instanciaValida(inst: String) = new ActionBuilder[Request] {
    def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) = {
      val ds = Try(DB.getDataSource(inst))
      if (ds.isFailure)
        Future.successful(BadRequest(Messages("instancia.invalido")))
      else
        block(request)
    }
  }

  def authenticated(inst: String) = new ActionBuilder[AuthenticatedDBRequest] {
    def invokeBlock[A](request: Request[A], block: (AuthenticatedDBRequest[A]) => Future[Result]) = {
      val res = for {
        instancia <- getDs(request) if inst.equals(instancia)
        validds <- Try(DB.getDataSource(inst)).toOption
      } yield {
        Database.forDataSource(validds).withTransaction { implicit s =>
          username(request).map { user =>
            block(new AuthenticatedDBRequest(user, s, request, instancia))
          }
        }
      }
      res.flatten.getOrElse(Future.successful(onUnauthorized(inst)))
    }
  }

  def hasPermission(proceso: String, tipos: Seq[String]) = new CustomActionRefiner[AuthenticatedDBRequest, PermisosRequest] {
    def refine[A](rs: AuthenticatedDBRequest[A]): Either[Result, PermisosRequest[A]] = {
      (for {
        idusuario <- rs.user.id
        if loginService.tienePermiso(proceso, tipos, idusuario)(rs.dbsession)
      } yield {
        PermisosRequest(proceso, tipos, rs)
      }) toRight (onInvalidCredentials(rs))
    }
  }

  def puedeInsertar(proceso: String) = new CustomActionRefiner[AuthenticatedDBRequest, InsercionRequest] {
    def refine[A](rs: AuthenticatedDBRequest[A]): Either[Result, InsercionRequest[A]] = {
      doRefine(rs, proceso, "insertar")(InsercionRequest(proceso, rs))
    }
  }
  def puedeActualizar(proceso: String) = new CustomActionRefiner[AuthenticatedDBRequest, ActualizacionRequest] {
    def refine[A](rs: AuthenticatedDBRequest[A]): Either[Result, ActualizacionRequest[A]] = {
      doRefine(rs, proceso, "actualizar")(ActualizacionRequest(proceso, rs))
    }
  }
  
  def puedeIngresar(proceso: String) = new CustomActionRefiner[AuthenticatedDBRequest, IngresoRequest] {
    def refine[A](rs: AuthenticatedDBRequest[A]): Either[Result, IngresoRequest[A]] = {
      doRefine(rs, proceso, "ingresar")(IngresoRequest(proceso, rs))
    }
  }

  def puedeConsultar(proceso: String) = new CustomActionRefiner[AuthenticatedDBRequest, ConsultaRequest] {
    def refine[A](rs: AuthenticatedDBRequest[A]): Either[Result, ConsultaRequest[A]] = {
      doRefine(rs, proceso, "consultar")(ConsultaRequest(proceso, rs))
    }
  }

  def puedeEliminar(proceso: String) = new CustomActionRefiner[AuthenticatedDBRequest, EliminacionRequest] {
    def refine[A](rs: AuthenticatedDBRequest[A]): Either[Result, EliminacionRequest[A]] = {
      doRefine(rs, proceso, "eliminar")(EliminacionRequest(proceso, rs))
    }
  }

  private def doRefine[A, B](rs: AuthenticatedDBRequest[A], proceso: String, permiso: String)(request: => B): Either[Result, B] = {
    (for {
      idusuario <- rs.user.id
      if loginService.tienePermiso(proceso, Seq(permiso), idusuario)(rs.dbsession)
    } yield {
      request
    }) toRight (onInvalidCredentials(rs))
  }

  //helpers/defaults
  def authAction(proceso: Proceso, tipo: String*) = (Authenticated andThen hasPermission(proceso, tipo.toSeq))

  def insertAction(proceso: Proceso) = (Authenticated andThen puedeInsertar(proceso))
  def updateAction(proceso: Proceso) = (Authenticated andThen puedeActualizar(proceso))
  def accessAction(proceso: Proceso) = (Authenticated andThen puedeIngresar(proceso))
  def queryAction(proceso: Proceso) = (Authenticated andThen puedeConsultar(proceso))
  def deleteAction(proceso: Proceso) = (Authenticated andThen puedeEliminar(proceso))
}