package controllers.control

import play.api.mvc._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.http.Status._
import models.services.RegistroActividadesService
import java.util.Date
import models.Tables._
import java.sql.Timestamp
import scala.util.Failure
import scala.util.Success
import play.api.Logger
import scala.language.postfixOps
import scala.annotation.tailrec

trait Registro {

  def registroService: RegistroActividadesService
  
  case class ApiError(message: String, friendlyMessage: String)
  case class ApiErrors(errors: Seq[ApiError], status: Int)

  case class Campo(nombre: String, valor: String, anterior: Option[String])

  sealed trait LogResult[T] {
    def value: T
    def registros: Seq[Campo]
  }

  trait InsercionResult[T] extends LogResult[T] {
    def response: Either[ApiErrors, InsercionResult[T]] = Right(this)
  }
  trait ActualizacionResult[T] extends LogResult[T] {
    def response: Either[ApiErrors, ActualizacionResult[T]] = Right(this)
  }
  trait ConsultaResult[T] extends LogResult[T] {
    def response: Either[ApiErrors, ConsultaResult[T]] = Right(this)
  }
  trait IngresoResult[T] extends LogResult[T] {
    def response: Either[ApiErrors, IngresoResult[T]] = Right(this)
  }
  trait EliminacionResult[T] extends LogResult[T]

  object InsercionResult {
    def apply[T](t: T, s: Product): InsercionResult[T] = new InsercionResult[T] {
      def value = t
      def registros = camposFromMap(getCCParams(s))
    }
  }

  object ActualizacionResult {
    def apply[T](t: T, s1: Product, s2: Product): ActualizacionResult[T] = new ActualizacionResult[T] {
      def value = t
      def registros = camposFromMap(getCCParams(s1), getCCParams(s2))
    }
  }

  object IngresoResult {
    def apply[T](t: T): IngresoResult[T] = new IngresoResult[T] {
      def value = t
      def registros = Seq()
    }
  }

  object ConsultaResult {
    def apply[T](t: T): ConsultaResult[T] = new ConsultaResult[T] {
      def value = t
      def registros = Seq()

    }
  }

  object EliminacionResult {
    def apply[T](t: T, s: Product): EliminacionResult[T] = new EliminacionResult[T] {
      def value = t
      def registros = camposFromMap(getCCParams(s))
      def response: Either[ApiErrors, EliminacionResult[T]] = Right(this)
    }
  }

  type InsercionResponse[T] = Either[ApiErrors, InsercionResult[T]]
  type ActualizacionResponse[T] = Either[ApiErrors, ActualizacionResult[T]]
  type ConsultaResponse[T] = Either[ApiErrors, ConsultaResult[T]]
  type IngresoResponse[T] = Either[ApiErrors, IngresoResult[T]]
  type EliminacionResponse[T] = Either[ApiErrors, EliminacionResult[T]]

  object ApiResponse extends Results {

    implicit val apiWrites = Json.writes[ApiError]

    //Semantica con implicits
    def apply[T](action: => InsercionResponse[T])(implicit rs: InsercionRequest[_], tjs: Writes[T]): Result = {
      process(action, "I")
    }

    def apply[T](action: => ActualizacionResponse[T])(implicit rs: ActualizacionRequest[_], tjs: Writes[T]): Result = {
      process(action, "A")
    }

    //Semantica explicita
    def apply[S, T](rs: InsercionRequest[S])(action: InsercionRequest[S] => InsercionResponse[T])(implicit tjs: Writes[T]): Result = {
      process(action(rs), "I")(rs, tjs)
    }

    def apply[S, T](rs: ActualizacionRequest[S])(action: ActualizacionRequest[S] => ActualizacionResponse[T])(implicit tjs: Writes[T]): Result = {
      process(action(rs), "A")(rs, tjs)
    }

    def apply[S, T](rs: ConsultaRequest[S])(action: ConsultaRequest[S] => ConsultaResponse[T])(implicit tjs: Writes[T]): Result = {
      process(action(rs), "C")(rs, tjs)
    }

    def apply[S, T](rs: EliminacionRequest[S])(action: EliminacionRequest[S] => EliminacionResponse[T])(implicit tjs: Writes[T]): Result = {
      process(action(rs), "E")(rs, tjs)
    }

    def apply[S, T](rs: IngresoRequest[S])(action: IngresoRequest[S] => IngresoResponse[T])(implicit tjs: Writes[T]): Result = {
      process(action(rs), "G")(rs, tjs) //G = Ingreso
    }

    private def process[T](block: => Either[ApiErrors, LogResult[T]], accion: String)(implicit rs: AccesosRequest[_], tjs: Writes[T]): Result = {
      block match {
        case Left(apiErrors) =>
          Status(apiErrors.status) {
            JsObject(Seq(
              "status" -> JsString("ERROR"),
              "statusCode" -> JsNumber(apiErrors.status),
              "errors" -> Json.toJson(apiErrors.errors)))
          }
        case Right(t) =>
          registrar(t.registros, accion)
          Ok {
            JsObject(Seq(
              "status" -> JsString("SUCCESS"),
              "response" -> Json.toJson(t.value)))
          }
      }
    }
    private lazy val logger: Logger = Logger(this.getClass())

    private def registrar(registros: Seq[Campo], accion: String)(implicit rs: AccesosRequest[_]) = {
      val fecha = new Timestamp(new Date().getTime())
      val idUser = rs.user.id.get

      val registro = RegistroActividadesRow(fecha, accion, rs.proceso, idUser)
      val camposRegistro = registros.map { c => CamposRegistroRow(c.nombre, c.valor, c.anterior, 0) }
      val saved = registroService.registrar(registro, camposRegistro)(rs.dbsession)
      saved match {
        case Failure(e)      => Logger.error("Error guardando registro de acciones", e)
        case Success((r, s)) => Logger.debug(s"Se ha guardado registro con id ${r.id.getOrElse(0)}")
      }
    }
  }

  def camposFromMap(m: Map[String, Any]): Seq[Campo] = {
    val res = m.map { case (k, v) => Campo(k, getString(v), None) }
    res.toSeq
  }

  def camposFromMap(m1: Map[String, Any], m2: Map[String, Any]): Seq[Campo] = {
    m1.map { case (k, v) => Campo(k, getString(v), m2.get(k).map(getString)) } toSeq
  }

  @tailrec
  final def getString(av: Any): String = av match {
    case Some(v) => getString(v)
    case _       => av.toString
  }

  def getCCParams(cc: Product) = cc.getClass.getDeclaredFields.map(_.getName) // all field names
    .zip(cc.productIterator.to).toMap // zipped with all values

  def toResponse[A](o: Option[A])(fm: => String, status: => Int, m: String = ""): Either[ApiErrors, A] = o match {
    case Some(a) => Right(a)
    case None    => Left(ApiErrors(List(ApiError(m, fm)), status))
  }

  def jsonToResponse[A, B](j: play.api.libs.json.JsResult[A]): Either[ApiErrors, A] =
    j.fold(
      errors => Left(ApiErrors(errors.map {
        case (jspath, validation) => ApiError("",
          validation.map(_.message).mkString(","))
      }, BAD_REQUEST)),
      value => Right(value))

  def messageError(status: Int)(m: String) = messagesError(m)(status)
  def messagesError(m: String*)(status: Int) =
    ApiErrors(m.map(x => ApiError(x, x)), status)
}
