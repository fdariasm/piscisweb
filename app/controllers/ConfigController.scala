package controllers

import play.api._
import play.api.mvc._
import controllers.control.Secured
import models.services.LoginService
import scala.slick.driver.MySQLDriver.simple._
import models.repositories.PermisosRepo
import models.repositories.LoginRepo
import models.services.LoginServiceImp
import controllers.control.Modulos
import controllers.helpers._

import play.api.libs.json._
import play.api.libs.json.JsValue

trait ConfigController { this: Controller with Secured with Modulos =>
  import FuncHelpers._
  
  val modulo = "Configuraciones"
    
  def configView(inst: String) = (authenticated(inst) andThen canAccessModule( modulo ) andThen WithModules){ implicit rs =>
    Ok(views.html.configMod()(Some(rs)))
  }
  
}

object ConfigController extends Controller with ConfigController with Secured with ValidUser with Modulos with ProdDep{
}