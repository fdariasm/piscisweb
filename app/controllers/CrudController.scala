package controllers

import models.domain.BaseEntity
import play.api.mvc._
import models.services.BaseService
import play.api.libs.json._
import play.api.libs.json.JsValue
import controllers.control._

trait CrudController[I, T <: BaseEntity[I]] { this: Controller with Registro =>
  import controllers.helpers.FuncHelpers._

  def modelService: BaseService[I, T]

  implicit def modelReads: play.api.libs.json.Reads[T]
  implicit def modelWrites: play.api.libs.json.OWrites[T]

  def msgSaved(model: T): String
  def msgUpdated(model: T): String
  def msgDeleted(model: T): String
  def msgNotFound(id: I): String

  protected def all[A]: IngresoRequest[A] => IngresoResponse[List[T]] = { //scala.slick.driver.MySQLDriver.simple.Session =>
    implicit rs =>
      IngresoResult(modelService.get).response
  }

  protected def create: InsercionRequest[JsValue] => InsercionResponse[String] = {
    implicit rs =>
      val modelJson = rs.body.validate[T]
      for {
        model <- jsonToResponse(modelJson).right
        saved <- modelService.add(model).left.map(messageError(BAD_REQUEST)).right
      } yield {
        InsercionResult(msgSaved(saved), saved)
      }
  }

  protected def update(id: I): ActualizacionRequest[JsValue] => ActualizacionResponse[String] = {
    implicit rs =>
      val model = rs.body.validate[T]

      for {
        fromDB <- toResponse(modelService.byId(id))(msgNotFound(id), BAD_REQUEST).right
        validM <- jsonToResponse(model).right
        updated <- modelService.update(validM).left.map(messageError(BAD_REQUEST)).right
      } yield {
        ActualizacionResult(msgUpdated(validM), fromDB, updated)
      }
  }

  protected def toEitherJson(id: I, message: String = "Not found")(implicit s: scala.slick.driver.MySQLDriver.simple.Session) =
    optionToEither(modelService.byId(id), Json.obj("status" -> "ERROR", "message" -> "Not found"))

  protected def get[A](id: I): ConsultaRequest[A] => ConsultaResponse[T] = {
    implicit rs =>
        modelService.byId(id).
          map(x => ConsultaResult(x)).
          toRight(messageError(BAD_REQUEST)(msgNotFound(id)))
  }

  protected def delete[A](id: I): EliminacionRequest[A] => EliminacionResponse[String] = {
    implicit rs =>
      for {
        model <- toResponse(modelService.byId(id))(msgNotFound(id), BAD_REQUEST).right
        _ <- modelService.delete(model).left.map(messageError(BAD_REQUEST)).right
      } yield {
        EliminacionResult(msgDeleted(model), model)
      }
  }
}