package controllers

import play.api._
import play.api.mvc._
import controllers.control.Secured
import models.services.LoginService
import scala.slick.driver.MySQLDriver.simple._
import models.Tables._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import models.services.LoginServiceImp
import models.repositories.PermisosRepo
import models.repositories.LoginRepo
import models.services.RolxusuarioService
import models.services.RolxusuarioServiceImp
import play.api.i18n.Messages
import models.repositories.RolesxusuarioRepo
import models.services.RolesService
import models.services.RolesServiceImp
import models.repositories.RolesRepo
import controllers.control.Registro

trait RolesxusuarioController { this: Controller with Secured with Registro =>
  import controllers.helpers.FuncHelpers._

  def rolxusuarioService: RolxusuarioService
  def rolesService: RolesService

  case class Rolxusuario(rpu: RolporusuarioRow, rol: RolesRow)

  implicit val rpuWrites = Json.writes[RolporusuarioRow]
  implicit val rolesWrites = Json.writes[RolesRow]

  implicit val modelWrites = Json.writes[Rolxusuario]

  val proceso = "rolporusuario"

  def jsonResponse(status: String)(msg: String) = Json.obj("status" -> status, "message" -> msg)

  val errorStatus = "ERROR"
  val okStatus = "OK"

  def rolesUsuario(iduser: Int) = accessAction(proceso) {
    ApiResponse(_) { implicit rs =>
      (for {
        _ <- loginService.byId(iduser)
        rolesUsuario = rolxusuarioService.rolesUsuario(iduser).map { case (rpu, rol) => Rolxusuario(rpu, rol) }.toList
      } yield {
        IngresoResult(rolesUsuario)
      }) toRight (messageError(BAD_REQUEST)(Messages("usuario.invalido")))
    }
  }

  def agregarRol(iduser: Int) = insertAction(proceso)(parse.json) {
    ApiResponse(_) { implicit rs =>
      val idrolJson = rs.body.validate[Int]
      for {
        user <- toResponse(loginService.byId(iduser))(Messages("usuario.invalido"), BAD_REQUEST).right
        idrol <- jsonToResponse(idrolJson).right
        _ <- toResponse(flipOption(rolxusuarioService.findByRol(iduser, idrol), ()))(
          Messages("rolxusuario.repetido"), BAD_REQUEST).right
        userAdd <- rolxusuarioService.add(RolporusuarioRow(idrol, iduser)).left.map(messageError(BAD_REQUEST)).right
      } yield {
        InsercionResult(Messages("rol.asignado"), userAdd)
      }
    }
  }

  def eliminarRol(idroluser: Int) = deleteAction(proceso) { 
    ApiResponse(_){implicit rs =>
    val proceso = for {
      rolxuser <- toResponse(rolxusuarioService.byId(idroluser))(Messages("rolxusuario.invalido"), BAD_REQUEST).right
      rol <- toResponse(rolesService.byId(rolxuser.idrol))(Messages("rol.invalido"), BAD_REQUEST).right
      _ <- rolxusuarioService.delete(rolxuser).left.map(messageError(INTERNAL_SERVER_ERROR)).right
    } yield {
      EliminacionResult(Messages("rolxusuario.eliminado").format(rol.nombre), rolxuser)
    }
    proceso
  }
  }
}

object RolesxusuarioController extends Controller with RolesxusuarioController with Secured with ValidUser with SecuredDep with Registro {
  lazy val rolxusuarioService: RolxusuarioService = new RolxusuarioServiceImp(new RolesxusuarioRepo {})
  lazy val rolesService: RolesService = new RolesServiceImp(new RolesRepo {})
}