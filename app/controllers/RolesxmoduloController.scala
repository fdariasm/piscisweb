package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.i18n.Messages
import controllers.control.{ Secured, Registro }
import models.Tables._
import scala.slick.driver.MySQLDriver.simple._
import models.services.ModulosService
import models.services.RolesxmoduloService
import models.services.RolesxmoduloServiceImp
import play.api.http.Status._
import models.services.RolesService
import models.services.RolesServiceImp
import models.repositories.RolesRepo

trait RolesxmoduloController { this: Controller with Secured with Registro =>
  import controllers.helpers.FuncHelpers._

  def rolesModuloService: RolesxmoduloService
  def modulosService: ModulosService
  def rolesService: RolesService

  lazy val proceso = "permisos_modulo"

  case class Rolxmodulo(rpm: PermisosModuloRow, rol: RolesRow)

  implicit val rolesWrites = Json.writes[RolesRow]
  implicit val modelWrites = Json.writes[PermisosModuloRow]
  implicit val rpmWrites = Json.writes[Rolxmodulo]

  def jsonResponse(status: String)(msg: String) = Json.obj("status" -> status, "message" -> msg)

  def rolesModulo(idModulo: Int) = accessAction(proceso) {
    ApiResponse(_) { implicit rs =>
      (for {
        modulo <- modulosService.byId(idModulo)
        rolesModulo = rolesModuloService.rolesModulo(idModulo)
      } yield {
        IngresoResult(rolesModulo.map(Rolxmodulo.tupled))
      }) toRight (messageError(BAD_REQUEST)(Messages("modulo.invalido")))
    }
  }

  def agregarPermiso(idmod: Int) = insertAction(proceso)(parse.json) {
    ApiResponse(_) { implicit rs =>
      for {
        modulo <- toResponse(modulosService.byId(idmod))(Messages("modulo.invalido"), BAD_REQUEST).right
        idrol <- jsonToResponse(rs.body.validate[Int]).right
        _ <- toResponse(flipOption(rolesModuloService.findByRol(idmod, idrol), ()))(
          Messages("rolxmodulo.repetido"), BAD_REQUEST).right
        p <- rolesModuloService.addPermiso(PermisosModuloRow(idrol, idmod)).left.map(messageError(BAD_REQUEST)).right
      } yield {
        InsercionResult(Messages("rol.asignado"), p)
      }
    }
  }

  def eliminarRol(idrolmodulo: Int) = deleteAction(proceso) {
    ApiResponse(_) { implicit rs =>
      for {
        rolxmod <- toResponse(rolesModuloService.rolesModuloById(idrolmodulo))(Messages("rolxmodulo.invalido"), BAD_REQUEST).right
        rol <- toResponse(rolesService.byId(rolxmod.rol))(Messages("rol.invalido"), BAD_REQUEST).right
        _ <- rolesModuloService.deleteRolesModulo(rolxmod).left.map(messageError(INTERNAL_SERVER_ERROR)).right
      } yield {
        EliminacionResult(Messages("rolxusuario.eliminado").format(rol.nombre), rolxmod)
      }
    }
  }

}

object RolesxmoduloController extends RolesxmoduloController with Controller with Secured with ValidUser with ProdDep with Registro {
  lazy val rolesModuloService = new RolesxmoduloServiceImp()
  lazy val rolesService: RolesService = new RolesServiceImp(new RolesRepo{})
}