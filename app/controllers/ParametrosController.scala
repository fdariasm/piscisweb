package controllers

import play.api._
import play.api.mvc._
import controllers.control.Secured
import models.services.LoginService
import scala.slick.driver.MySQLDriver.simple._
import models.repositories.PermisosRepo
import models.repositories.LoginRepo
import models.services.LoginServiceImp
import models.services.ParametrosService
import models.repositories.ParametrosRepo
import models.services.ParametrosServiceImp
import models.Tables._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import controllers.control.Registro
import play.api.i18n.Messages

trait ParametrosController extends CrudController[Int, ParametrosRow] { this: Controller with Secured with Registro =>
  import controllers.helpers.FuncHelpers._

  implicit val modelReads = Json.reads[ParametrosRow]
  implicit val modelWrites = Json.writes[ParametrosRow]

  def parametrosService: ParametrosService

  def modelService = parametrosService

  def msgSaved(model: ParametrosRow): String = s"Parametro guardado"
  def msgUpdated(model: ParametrosRow): String = s"Parametro ${model.id} actualizado"
  def msgDeleted(model: ParametrosRow): String = s"Parametro ${model.id} eliminado"
  def msgNotFound(id: Int) = Messages("parametro.invalido")

  val proceso = "parametros"

  def addParam = insertAction(proceso)(parse.json) {
    ApiResponse(_) { create }
  }

  def updateParam(id: Int) = updateAction(proceso)(parse.json) {
    ApiResponse(_) { update(id) }
  }

  def deleteParam(id: Int) = deleteAction(proceso) {
    ApiResponse(_) { delete(id) }
  }

  def getParam(id: Int) = queryAction(proceso) {
    ApiResponse(_) { get(id) }
  }

  def getParametros = accessAction(proceso) {
    ApiResponse(_) { all }
  }
}

object ParametrosController extends Controller with ParametrosController with Secured with ValidUser with SecuredDep with Registro{
  val parametrosService: ParametrosService = new ParametrosServiceImp(new ParametrosRepo {})
}