package controllers.helpers

import play.api.data.Form
import play.api.libs.json.JsError
import play.api.libs.json.Json
import java.sql.Timestamp
import java.text.SimpleDateFormat
import play.api.mvc.Result
import play.api.mvc.Results._
import controllers.control.{AuthenticatedDBRequest, WithModulesRequest}
import controllers.control.PermisosRequest
import controllers.control.AccesosRequest

object FuncHelpers {
  def defOption[A, B](o: Option[A], b: => B)(f: A => B): Option[B] = o match {
    case Some(a) => Some(f(a))
    case None    => Some(b)
  }

  val dateFormater = new SimpleDateFormat("yyyy-MM-dd")

  def getTimestamp(s: String): Timestamp = {
    val d = dateFormater.parse(s);
    new Timestamp(d.getTime)
  }

  def optionToEither[A, B](o: Option[A], v: => B): Either[B, A] = o match {
    case Some(a) => Right(a)
    case None    => Left(v)
  }

  def formToEither[A, B](f: Form[A])(g: => Form[A] => B): Either[B, A] = f.fold(
    fwe => Left(g(fwe)),
    a => Right(a))

  type JsonError = Seq[(play.api.libs.json.JsPath, Seq[play.api.data.validation.ValidationError])]

  def jsonToEither[A, B](j: play.api.libs.json.JsResult[A])(f: JsonError => B) =
    j.fold(
      errors => Left(f(errors)),
      value => Right(value))

  def defaultJsonEither[A](j: play.api.libs.json.JsResult[A]): Either[Result, A] =
    jsonToEither(j)(errors => BadRequest(jsonValidateError(errors)))

  def jsonValidateError(errors: JsonError) =
    Json.obj("status" -> "ERROR", "message" -> JsError.toFlatJson(errors))

  def getCCParams(cc: AnyRef) =
    (Map[String, Any]() /: cc.getClass.getDeclaredFields) { (a, f) =>
      f.setAccessible(true)
      a + (f.getName -> f.get(cc))
    }
 
  def booleanToOption[T](v: Boolean, e: => T): Option[T] =
    if (v) Some(e) else None

  implicit def sessionFromADBR[A](implicit rs: AuthenticatedDBRequest[A]): scala.slick.driver.MySQLDriver.simple.Session =
    rs.dbsession
    
  implicit def sessionFromPR[A](implicit rs: PermisosRequest[A]): scala.slick.driver.MySQLDriver.simple.Session =
    rs.dbsession
    
  implicit def sessionFromAR[A](implicit rs: AccesosRequest[A]): scala.slick.driver.MySQLDriver.simple.Session =
    rs.dbsession

  def pairsToMapSet[A, B](seq: Seq[(A, B)]): Map[A, Set[B]] = {
    def iterate(ls: Seq[(A, B)], acc: Map[A, Set[B]]): Map[A, Set[B]] = ls match {
      case Seq() => acc
      case Seq((pr, tp), ns @ _*) => {
        val s = acc.getOrElse(pr, Set[B]())
        iterate(ns, acc ++ Map(pr -> (s + tp)))
      }
    }
    iterate(seq, Map())
  }

  def flipOption[A, B](o: Option[A], v: B): Option[B] = o match {
    case Some(a) => None
    case None    => Some(v)
  }
}