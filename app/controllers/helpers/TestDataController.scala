package controllers.helpers

import play.api._
import play.api.mvc._
import scala.slick.driver.MySQLDriver.simple._
import play.api.Play.current
import play.api.db.DB
import models.TestData
import scala.util.Try

import javax.sql.DataSource

object TestDataController extends Controller {

  def insertInitData(inst: String) = Action { rs =>
    val tryds: Try[DataSource] = Try(DB.getDataSource(inst))
    tryds.map { dataSource =>
      Database.forDataSource(dataSource).withTransaction { implicit s =>
        TestData.insertTestData.map(_ => Ok("Datos insertados correctamente")).recover {
          case f => InternalServerError("Error en la insercion: " + f.getMessage())
        }.get
      }
    }.getOrElse(BadRequest("Instancia inválida"))
  }
}