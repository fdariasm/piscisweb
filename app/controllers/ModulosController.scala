package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.i18n.Messages

import controllers.control.Secured
import models.Tables._
import scala.slick.driver.MySQLDriver.simple._
import models.services.ModulosService
import controllers.control._

trait ModulosController extends CrudController[Int, ModulosRow] { this: Controller with Secured with Registro =>
  import controllers.helpers.FuncHelpers._
  implicit val modelReads = Json.reads[ModulosRow]
  implicit val modelWrites = Json.writes[ModulosRow]

  def modulosService: ModulosService
  def modelService = modulosService

  def msgSaved(model: ModulosRow): String = Messages("modulo.guardado")
  def msgUpdated(model: ModulosRow): String = Messages("modulo.actualizado")
  def msgDeleted(model: ModulosRow): String = Messages("modulo.eliminado")
  def msgNotFound(id: Int) = Messages("modulo.invalido")

  val proceso = "modulos"

  def addModulo = insertAction(proceso)(parse.json) {
    ApiResponse(_) { create }
  }
  def updateModulo(id: Int) = updateAction(proceso)(parse.json) {
    ApiResponse(_) { update(id) }
  }

  def deleteModulo(id: Int) = deleteAction(proceso) {
    ApiResponse(_) { delete(id) }
  }

  def getModulo(id: Int) = queryAction(proceso) {
    ApiResponse(_) { get(id) }
  }

  def getModulos = accessAction(proceso) {
    ApiResponse(_) { all }
  }

}

object ModulosController extends Controller with ModulosController with Secured with ValidUser with ProdDep with Registro {

}