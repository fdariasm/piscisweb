package controllers

import play.api._
import play.api.mvc._
import play.api.data.Forms._
import play.api.data._
import models.services.LoginServiceImp
import models.services.LoginService
import models.repositories.PermisosRepo
import models.repositories.LoginRepo
import scala.slick.driver.MySQLDriver.simple._
import play.api.Play.current
import play.api.db.DB
import models.Tables.UsuariosRow
import java.util.Date
import scala.util.Try
import javax.sql.DataSource
import controllers.control.Secured
import play.api.i18n.Messages

trait LoginController { this: Controller with Secured =>

  val loginForm = Form {
    tuple(
      "username" -> nonEmptyText,
      "password" -> nonEmptyText)
  }

  def login(instancia: String) = Action { implicit request =>
    val tryds: Try[DataSource] = Try(DB.getDataSource(instancia))
    tryds.map(_ => Ok(views.html.login(loginForm, instancia))).
      getOrElse(BadRequest("Instancia inválida")) //TODO diseñar pag de error
  }

  def authenticate(instancia: String) = Action { implicit request =>
    val tryds: Try[DataSource] = Try(DB.getDataSource(instancia))
    tryds.map { datasource =>
      Database.forDataSource(datasource).withTransaction { implicit s =>
        loginForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.login(formWithErrors, instancia)),
          user => {
            if (loginService.checkCredentials(user._1, user._2))
              Redirect(routes.MainController.vistaPrinc(instancia)).withSession(Security.username -> user._1, dsKey -> instancia)
            else {
              BadRequest(views.html.login(loginForm.fill(user).withGlobalError(Messages("login.error")), instancia))
            }
          })
      }
    } getOrElse {
      BadRequest("Instancia inválida")//TODO diseñar pag de error
    }
  }

  def logout = Authenticated { rs =>
    Redirect(routes.LoginController.login(rs.datasource)).withNewSession.flashing(
      "success" -> "Logged out")
  }

}

object LoginController extends LoginController with Controller with Secured with ValidUser with SecuredDep{
}