package controllers

import play.api._
import play.api.mvc._
import models.services.LoginService
import scala.slick.driver.MySQLDriver.simple._
import models.repositories.PermisosRepo
import models.repositories.LoginRepo
import models.services.LoginServiceImp
import models.services.ModulosServiceImp
import models.repositories.ModulosRepo
import models.services.ModulosService
import scala.slick.driver.MySQLDriver.simple.Session
import scala.slick.driver.MySQLDriver.simple.Database
import models.Tables._
import controllers.control._
import java.util.Date
import models.services.{ RegistroActividadesService, RegistroActividadesServiceImp }
import scala.util.Try
import scala.util.Success

trait SecuredDep { this: Secured =>
  val dsKey = "datasource"
  lazy val permisosRepo = new PermisosRepo {}
  lazy val loginService: LoginService = new LoginServiceImp(new LoginRepo {}, permisosRepo)

  lazy val registroService: RegistroActividadesService = if(play.api.Play.isDev(play.api.Play.current)) 
    fakeRegistry else new RegistroActividadesServiceImp()

  private val logger: Logger = Logger("fakeRegistry")
  object fakeRegistry extends RegistroActividadesService {
    def registrar(registro: RegistroActividadesRow,
                  campos: Seq[CamposRegistroRow])(implicit s: Session): Try[Registro] = {
      Logger.debug(s"FakeRegistry: $registro") //cambiar default logger
      Success((registro, campos))
    }
  }
}

trait ProdDep extends SecuredDep { this: Secured =>
  lazy val modulosService: ModulosService = new ModulosServiceImp(new ModulosRepo {}, permisosRepo)
}

trait ValidUser { this: Controller =>
  def loginService: LoginService

  def username(request: RequestHeader)(implicit s: Session) = request.session.get(Security.username).flatMap(loginService.getUser(_))
  //    def onUnauthorized(request: RequestHeader)(implicit s: Session): Result = Unauthorized("Debe autenticarse para proceder")
}

trait TestingSecured extends Secured { this: Controller =>

  val defaultUser = UsuariosRow("0", "admin", "admin", "email", "qwerty", "qwerty", Some(new java.sql.Timestamp(new Date().getTime())));

  def username(request: RequestHeader)(implicit s: Session) = Some(defaultUser);

  //    def onUnauthorized(request: RequestHeader)(implicit s: Session) = Unauthorized("Debe autenticarse para proceder")
}