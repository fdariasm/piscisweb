package models

import scala.slick.driver.JdbcDriver.simple._

case class DBReader[A](val t: Int)(val g: Session => A) {
  def apply(s: Session) = {
    g(s)
    println(t) //se guardaria a bd en su lugar
  }
  def map[B](f: A => B): DBReader[B] =
    DBReader(t) { s => f(g(s)) }

  def flatMap[B](f: A => DBReader[B]): DBReader[B] =
    DBReader(t) { s => f(g(s)).g(s) }
}