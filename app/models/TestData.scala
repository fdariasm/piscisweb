package models

import scala.slick.driver.MySQLDriver.simple._
import models.Tables._
import scala.util.Try
import java.util.Date
import org.mindrot.jbcrypt.BCrypt

object TestData {

  def insertTestData(implicit s: Session): Try[Unit] = Try {
    Permisosxrol.delete.run
    Rolporusuario.delete.run
    PermisosModulo.delete.run
    Usuarios.delete.run
    Roles.delete.run
    Procesos.delete.run
    TipoPermiso.delete.run
    Modulos.delete.run

    val claveAdmin = BCrypt.hashpw("qwerty", BCrypt.gensalt())
    val claveUser = BCrypt.hashpw("qwerty", BCrypt.gensalt())
    Usuarios.map(u => (u.identificacion, u.usuario, u.nombre, u.email, u.clave1, u.clave2, u.reiniciarclave1, u.reiniciarclave2)) ++= Seq(
      ("0", "admin", "admin", "correo@correo.com", claveAdmin, claveAdmin, false, false),
      ("123", "user", "user", "correo1@correo.com", claveUser, claveUser, false, false))

    Modulos.map(_.nombre) ++= Seq("Principal", "Configuraciones")

    val mod = Modulos.filter(_.nombre === "Configuraciones").firstOption
    mod.map { m =>
      val idmodulo = m.id.get
      Procesos.map(p => (p.nombre, p.modulo)) ++= Seq(
        ("usuarios", idmodulo),
        ("roles", idmodulo),
        ("modulos", idmodulo),
        ("rolporusuario", idmodulo),
        ("parametros", idmodulo),
        ("permisos_modulo", idmodulo))
    }

    val tiposPermiso = Set("ingresar", "insertar", "actualizar", "eliminar", "consultar")

    TipoPermiso.map(_.nombre) ++= tiposPermiso

    Roles.map(_.nombre) ++= Seq("usuario", "admin")

    val rolAdmin = Roles.filter(_.nombre === "admin").firstOption
    val userAdmin = Usuarios.filter(_.identificacion === "0").firstOption
    rolAdmin.flatMap(rol => userAdmin.map { user =>
      Rolporusuario.map(r => (r.idrol, r.idusuario)).insert((rol.id.get, user.id.get))
    })

    val procesoUsuarios = Procesos.filter(_.nombre === "usuarios").firstOption
    val permisos = TipoPermiso.filter(_.nombre inSet tiposPermiso).list

    val moduloConf = Modulos.filter(_.nombre === "Configuraciones").firstOption
    for {
      admin <- rolAdmin
      conf <- moduloConf
    } yield {
      PermisosModulo += PermisosModuloRow(admin.id.get, conf.id.get)
    }

    val moduloPrinc = Modulos.filter(_.nombre === "Principal").firstOption

    for {
      admin <- rolAdmin
      principal <- moduloPrinc
    } yield {
      PermisosModulo += PermisosModuloRow(admin.id.get, principal.id.get)
    }

    for {
      admin <- rolAdmin.toList
      usuarios <- procesoUsuarios.toList
      permiso <- permisos
    } yield {
      Permisosxrol += PermisosxrolRow(permiso.id.get, usuarios.id.get, admin.id.get)
    }

    val procesoRoles = Procesos.filter(_.nombre === "roles").firstOption

    for {
      admin <- rolAdmin.toList
      roles <- procesoRoles.toList
      permiso <- permisos
    } yield {
      Permisosxrol += PermisosxrolRow(permiso.id.get, roles.id.get, admin.id.get)
    }

    val procesoModulos = Procesos.filter(_.nombre === "modulos").firstOption

    for {
      admin <- rolAdmin.toList
      modulos <- procesoModulos.toList
      permiso <- permisos
    } yield {
      Permisosxrol += PermisosxrolRow(permiso.id.get, modulos.id.get, admin.id.get)
    }

    val procesoRolesusuario = Procesos.filter(_.nombre === "rolporusuario").firstOption

    for {
      admin <- rolAdmin.toList
      rolesUsuario <- procesoRolesusuario.toList
      permiso <- permisos
    } yield {
      Permisosxrol += PermisosxrolRow(permiso.id.get, rolesUsuario.id.get, admin.id.get)
    }

    val procesoParametros = Procesos.filter(_.nombre === "parametros").firstOption

    for {
      admin <- rolAdmin.toList
      parametros <- procesoParametros.toList
      permiso <- permisos
    } yield {
      Permisosxrol += PermisosxrolRow(permiso.id.get, parametros.id.get, admin.id.get)
    }
    val procesoPermisosModulo = Procesos.filter(_.nombre === "permisos_modulo").firstOption

    for {
      admin <- rolAdmin.toList
      permisosModulo <- procesoPermisosModulo.toList
      permiso <- permisos
    } yield {
      Permisosxrol += PermisosxrolRow(permiso.id.get, permisosModulo.id.get, admin.id.get)
    }
  }

}