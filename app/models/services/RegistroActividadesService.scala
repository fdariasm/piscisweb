package models.services

import scala.slick.driver.JdbcDriver.simple._
import models.Tables._
import scala.util.Try

trait RegistroActividadesService {
  
  type Registro = (RegistroActividadesRow, Seq[CamposRegistroRow])
  
  def registrar(registro: RegistroActividadesRow, 
      campos: Seq[CamposRegistroRow])(implicit s: Session): Try[Registro]
}

class RegistroActividadesServiceImp extends DBCrud with RegistroActividadesService{
  val profile = scala.slick.driver.MySQLDriver
  import profile.simple._

  def registrar(registro: RegistroActividadesRow, 
      campos: Seq[CamposRegistroRow])(implicit s: Session) = {
    Try(
      s.withTransaction{
        val idRegistro = RegistroActividades.returning(RegistroActividades.map(_.id)).insert(registro) 
        val savedCampos = for{
          campo <- campos
        }yield{
          val idCampo = CamposRegistro.returning(CamposRegistro.map(_.id)).insert(campo.copy(registroActividades = idRegistro))
          campo.copy(id = Some(idCampo))
        }
        (registro.copy(id = Some(idRegistro)), savedCampos)
      }
    )
  }
}