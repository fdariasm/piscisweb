package models.services

import models.domain.BaseEntity
import models.repositories.BaseRepo

trait BaseService[I, T <: BaseEntity[I]] {
  val profile: scala.slick.driver.JdbcDriver
  import profile.simple._

  def repository: BaseRepo[I, T]

  def add(model: T)(implicit s: Session): Either[String, T] = { 
    try {
      Right(repository.add(model).run(s))
    } catch {
      case e: Throwable => Left(e.getMessage())
    }
  }

  def update(model: T)(implicit s: Session): Either[String, T] = {
    try {
      Right(repository.update(model).run(s))
    } catch {
      case e: Throwable => Left(e.getMessage())
    }
  }

  def get(implicit s: Session): List[T] = {
    repository.list.run(s)
  }

  def byId(id: I)(implicit s: Session): Option[T] = {
    repository.findById(id).run(s)
  }

  def delete(model: T)(implicit s: Session): Either[String, Unit] = {
    try {
      Right(repository.delete(model).run(s))
    } catch {
      case e: Throwable => Left(e.getMessage())
    }
  }
}

