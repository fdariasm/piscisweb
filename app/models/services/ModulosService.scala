package models.services

import models.Tables._
import scala.slick.driver.MySQLDriver.simple._
import models.domain.MySqlProfile
import models.repositories.ModulosRepo
import models.repositories.PermisosRepo

trait ModulosService extends BaseService[Int, ModulosRow] {
  def modulosByUser(iduser: Int)(implicit s: Session): Seq[ModulosRow]
  
  def canAccess( iduser: Int, modulo: String )(implicit s: Session): Boolean
  
  def getProcesosxModuloUser( iduser: Int, modulo: String )(implicit s: Session): Seq[(ProcesosRow, TipoPermisoRow)]
}

class ModulosServiceImp(val repository: ModulosRepo, permisosRepo: PermisosRepo) extends ModulosService with MySqlProfile {
  def modulosByUser(iduser: Int)(implicit s: Session): Seq[ModulosRow] =
    repository.modulosByUser(iduser).run
    
  def canAccess( iduser: Int, modulo: String )( implicit s: Session ): Boolean = {
    val idrolxuser = permisosRepo.getRolesxUsuario(iduser).map(_.id)
    repository.getRolesxModule(modulo).filter(_.id in idrolxuser).exists.run
  }
  
//  def procesosModuloUser( iduser: Int, modulo: String)( implicit s: Session ) = {
//    val procMod = repository.getProcesosxModulo(modulo, iduser).map(_.id)
//    val rolesUser = permisosRepo.getRolesxUsuario(iduser).map(_.id)
//    for{
//      ppr <- Permisosxrol if (ppr.rol in rolesUser) && (ppr.procesos in procMod)
//    } yield (ppr)
//  }
  
  def getProcesosxModuloUser( iduser: Int, modulo: String )(implicit s: Session) = {
    repository.getProcesosxModuloUser(modulo, iduser).run
  }
}