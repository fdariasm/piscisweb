package models.services

import scala.slick.driver.JdbcDriver.simple._

import models.domain.BaseEntity
import models.domain.TableWithId

trait DBCrud {
  val profile: scala.slick.driver.JdbcDriver
  import profile.simple._
  
  protected def add[I: BaseColumnType, T <: BaseEntity[I]](
    query: TableQuery[_ <: Table[T] with TableWithId[I]])(
        model: T)(withId: I => T => T)(implicit s: Session): Either[String, T] = {
    try {
      val id = query.returning(query.map(_.id)).insert(model)
      Right(withId(id)(model))
    } catch {
      case e: Exception => {
        s.rollback
        Left(e.getMessage())
      }
    }
  }
  
  protected def filterById[I: BaseColumnType, T <: BaseEntity[I]](
    query: TableQuery[_ <: Table[T] with TableWithId[I]])(id: I) = query.filter(_.id === id)

  protected def update[I: BaseColumnType, T <: BaseEntity[I]](
    query: TableQuery[_ <: Table[T] with TableWithId[I]])(
        model: T)(implicit s: Session): Either[String, T] = {
    try {
      val res = filterById(query)(model.id.get).update(model).run
      Right(model)
    } catch {
      case e: Exception => Left(e.getMessage())
    }
  }
  
  protected def delete[I: BaseColumnType, T <: BaseEntity[I]](
    query: TableQuery[_ <: Table[T] with TableWithId[I]])(
        model: T)(implicit s: Session): Either[String, Unit] = {
    deleteById(query)(model.id.get)
  }
  
  protected def deleteById[I: BaseColumnType, T <: BaseEntity[I]](
    query: TableQuery[_ <: Table[T] with TableWithId[I]])(
        id: I)(implicit s: Session): Either[String, Unit] = {
    try {
      Right(filterById(query)(id).delete)
    }catch{
      case e: Exception => Left( e.getMessage )
    }
  }
  
  protected def findById[I: BaseColumnType, T <: BaseEntity[I]](
    query: TableQuery[_ <: Table[T] with TableWithId[I]])(
        id: I)(implicit s: Session): Option[T] = {
    filterById(query)(id).firstOption
  }
}