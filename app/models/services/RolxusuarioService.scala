package models.services

import models.Tables._
import scala.slick.driver.MySQLDriver.simple._
import models.domain.MySqlProfile
import models.repositories.RolesxusuarioRepo

trait RolxusuarioService extends BaseService[Int, RolporusuarioRow] {
  def rolesUsuario(iduser: Int)(implicit s: Session): Seq[(RolporusuarioRow, RolesRow)]
  
  def findByRol(iduser: Int, idrol: Int)(implicit s: Session): Option[RolporusuarioRow]
}

class RolxusuarioServiceImp(val repository: RolesxusuarioRepo) extends RolxusuarioService with MySqlProfile{

  def rolesUsuario(iduser: Int)(implicit s: Session) = {
    repository.rolesUsuario(iduser).run
  }
  
  def findByRol(iduser: Int, idrol: Int)(implicit s: Session): Option[RolporusuarioRow] = {
    repository.findByRol(iduser, idrol).firstOption
  }
}