package models.services

import models.Tables._
import play.api.db._
import scala.slick.jdbc.JdbcBackend.Database
import scala.slick.driver.MySQLDriver.simple._
import javax.sql.DataSource
import org.mindrot.jbcrypt.BCrypt
import models.repositories.LoginRepo
import play.api.Play
import models.domain.MySqlProfile
import models.repositories.PermisosRepo
import scalaz.Reader

trait LoginService extends BaseService[Int, UsuariosRow] {

  def checkCredentials(username: String, password: String)(implicit s: Session): Boolean

  def userExists(username: String)(implicit s: Session): Boolean

  def getUser(username: String)(implicit s: Session): Option[UsuariosRow]

  def getUser(id: Int)(implicit s: Session): Option[UsuariosRow]

  def createAdmin(implicit s: Session): Unit

  def getRoles(user: UsuariosRow)(implicit s: Session): Seq[RolesRow]
    
  def tienePermiso(proceso: String, tipos: Seq[String], idusuario: Int)(implicit s: Session): Boolean
}

class LoginServiceImp(val repository: LoginRepo, val permisoRepo: PermisosRepo) extends LoginService with MySqlProfile {

  def createAdmin(implicit s: Session) = {
    if (!userExists("admin")) {
      val pass = Play.current.configuration.getString("admin.password")
      pass.map { p =>
        add(UsuariosRow("0", "admin", "", "", p, p))
      }
    }
  }

  def checkCredentials(username: String, password: String)(implicit s: Session): Boolean = {
    repository.checkCredentials(username, password).apply(s)
  }

  override def add(user: UsuariosRow)(implicit s: Session) = {
    try {
      val userEncripted = user.copy(clave1 = createPasswd(user.clave1))
      Right(repository.add(userEncripted).apply(s))
    } catch {
      case e: Throwable => Left(e.getMessage())
    }
  }

  def createPasswd(clearString: String) = {
    BCrypt.hashpw(clearString, BCrypt.gensalt())
  }

  def userExists(username: String)(implicit s: Session) = {
    repository.userExists(username).apply(s)
  }

  def getUser(username: String)(implicit s: Session): Option[UsuariosRow] = {
    repository.getUser(username).apply(s)
  }

  def getUser(id: Int)(implicit s: Session): Option[UsuariosRow] = repository.findById(id).apply(s)

  def getRoles(user: UsuariosRow)(implicit s: Session): Seq[RolesRow] =
    user.id.map(repository.getRoles(_).run(s)).getOrElse(Seq())

  
  def tienePermiso(proceso: String, tipos: Seq[String], idusuario: Int)(implicit s: Session): Boolean = {
    val idrolxUser = permisoRepo.getRolesxUsuario(idusuario).map(_.id)
    permisoRepo.getRolesxPermiso(proceso, tipos).filter(_.id in idrolxUser).exists.run
  }
}
    
