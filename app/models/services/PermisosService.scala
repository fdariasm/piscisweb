package models.services

import models.Tables._
import scala.slick.driver.MySQLDriver.simple._
import models.domain.MySqlProfile

import models.repositories.PermisosRepo

trait PermisosService extends BaseService[Int, PermisosxrolRow]

class PermisosServiceImp(val repository: PermisosRepo) extends PermisosService with MySqlProfile {
  
}

