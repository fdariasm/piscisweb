package models.services

import models.Tables._
import scala.slick.driver.JdbcDriver.simple._

trait RolesxmoduloService {
  def rolesModulo(idmodulo: Int)(implicit s: Session): Seq[(PermisosModuloRow, RolesRow)]
  def findByRol(idmodulo: Int, idrol: Int)(implicit s: Session): Option[PermisosModuloRow]
  def addPermiso(permiso: PermisosModuloRow)(implicit s: Session): Either[String, PermisosModuloRow]
  def rolesModuloById(idrolesModulo: Int)(implicit s: Session): Option[PermisosModuloRow]
  def deleteRolesModulo(rolesModulo: PermisosModuloRow)(implicit s: Session): Either[String, Unit]
}

class RolesxmoduloServiceImp extends RolesxmoduloService with DBCrud{
  val profile = scala.slick.driver.MySQLDriver
  import profile.simple._
  
  def rolesModulo(idmodulo: Int)(implicit s: Session) = {
    val query = for{
      rpm <- PermisosModulo if rpm.modulo === idmodulo
      rol <- rpm.rolesFk
    } yield (rpm, rol)
    query.run
  }
  
  def findByRol(idmodulo: Int, idrol: Int)(implicit s: Session): Option[PermisosModuloRow] = {
    PermisosModulo.filter{p => p.rol === idrol && p.modulo === idmodulo}.firstOption
  }
  
  def addPermiso(permiso: PermisosModuloRow)(implicit s: Session): Either[String, PermisosModuloRow] = {
    add(PermisosModulo)(permiso)(id => p => p.copy(id = Some(id)))
  }
  
  def rolesModuloById(idrolesModulo: Int)(implicit s: Session): Option[PermisosModuloRow] = {
    findById(PermisosModulo)(idrolesModulo)
  }
  
  def deleteRolesModulo(rolesModulo: PermisosModuloRow)(implicit s: Session): Either[String, Unit] =
    delete(PermisosModulo)(rolesModulo)
}