package models.services

import models.Tables._
import scala.slick.driver.MySQLDriver.simple._
import models.domain.MySqlProfile
import models.repositories.RolesRepo

trait RolesService extends BaseService[Int, RolesRow]

class RolesServiceImp(val repository: RolesRepo) extends RolesService with MySqlProfile {
  
}