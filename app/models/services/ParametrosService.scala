package models.services

import models.Tables._
import scala.slick.driver.MySQLDriver.simple._
import models.domain.MySqlProfile
import models.repositories.ParametrosRepo

trait ParametrosService extends BaseService[Int, ParametrosRow]

class ParametrosServiceImp(val repository: ParametrosRepo) extends ParametrosService with MySqlProfile {

}