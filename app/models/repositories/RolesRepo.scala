package models.repositories

import models.Tables._
import models.domain.MySqlProfile

trait RolesRepo extends BaseRepo[Int, RolesRow] with MySqlProfile {
  import profile.simple._
  
  val query = Roles

  def withId(model: RolesRow, id: Int) = model.copy(id = Some(id))

}