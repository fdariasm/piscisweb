package models.repositories

import models.Tables._
import models.domain.MySqlProfile
import scalaz.Reader

trait PermisosRepo extends BaseRepo[Int, PermisosxrolRow] with MySqlProfile {
  import profile.simple._

  val query = Permisosxrol

  def withId(model: PermisosxrolRow, id: Int) = model.copy(id = Some(id))

  def getPermisos(proceso: String, permisos: Set[String]) = {
    for {
      perm <- Permisosxrol
      proc <- perm.procesosFk if proc.nombre === proceso
      tipo <- perm.tipoPermisoFk if tipo.nombre inSet permisos
    } yield (perm)
  }

  def getRolesxUsuario(iduser: Int) = {
    for {
      rpu <- Rolporusuario if rpu.idusuario === iduser
      rol <- rpu.rolesFk
    } yield (rol)
  }

  def getRolesxPermiso(proceso: String, tipos: Seq[String]) = {
    for {
      pxr <- Permisosxrol
      prc <- pxr.procesosFk if prc.nombre === proceso
      tipo <- pxr.tipoPermisoFk if tipo.nombre inSet tipos
      rol <- pxr.rolesFk
    } yield {
      rol
    }
  }

  //  def findByProceso(proceso: String, tipos: Set[String], iduser: Int) = {
  //    val roleuser = for {
  //      rpu <- Rolporusuario if rpu.idusuario === iduser
  //      rol <- rpu.rolesFk
  //    } yield (rol.id)
  //
  //    val rolespermiso = for {
  //      p <- query if p.proceso === proceso
  //      tipo <- p.tipopermisoFk if tipo.tipo inSet tipos
  //      pxr <- Permisoxrol if pxr.permiso === p.id
  //      rol <- pxr.rolesFk if rol.id in roleuser
  //    } yield {
  //      rol
  //    }
  //    rolespermiso
  //  }
  //
  //
  //
  //  def getRolesxTipo(tipoPerm: String) = {
  //    for {
  //      p <- query
  //      tipo <- p.tipopermisoFk if tipo.tipo === tipoPerm
  //      pxr <- Permisoxrol if pxr.permiso === p.id
  //      rol <- pxr.rolesFk
  //    } yield {
  //      rol
  //    }
  //  }
  //
  //  def getModulosUsuario(iduser: Int) = {
  //    for {
  //      ((((rpu, rol), ppr), perm), tipop) <- Rolporusuario innerJoin Roles innerJoin Permisoxrol innerJoin Permisos innerJoin Tipopermiso
  //      if rpu.idusuario === iduser && tipop.tipo === "modulo"
  //    } yield (perm)
  //  }

}