package models.repositories

import models.Tables._
import models.domain.MySqlProfile
import org.mindrot.jbcrypt.BCrypt
import scalaz.Reader

trait LoginRepo extends BaseRepo[Int, UsuariosRow] with MySqlProfile {
  import profile.simple._

  val query = Usuarios

  def withId(model: UsuariosRow, id: Int) = model.copy(id = Some(id))

  def checkCredentials(username: String, password: String): Reader[Session, Boolean] = Reader {
    implicit s =>
      query.filter(_.usuario === username).
        map(_.clave1).
        run.
        exists(checkPassword(_, password))
  }

  def userExists(username: String): Reader[Session, Boolean] = Reader {
    implicit s =>
      query.filter(_.usuario === username).exists.run
  }

  def checkPassword(encrypted: String, userPass: String) = {
    BCrypt.checkpw(userPass, encrypted);
  }

  def getUser(username: String): Reader[Session, Option[UsuariosRow]] = Reader {
    implicit s =>
      query.filter(_.usuario === username).firstOption
  }

  def getRoles(iduser: Int): Reader[Session, Seq[RolesRow]] = Reader { implicit s =>
    val res = for {
      rpu <- Rolporusuario if (rpu.idusuario === iduser)
      rol <- rpu.rolesFk
    } yield (rol)
    res.run
  }

  override def update(user: UsuariosRow): Reader[Session, UsuariosRow] = Reader { implicit s =>
    println("user")
    println(user)
    val res = query.filter(_.id === user.id.get).map(u => 
      (u.identificacion, u.usuario, u.nombre, u.email, u.fecharetiro, u.reiniciarclave1, u.reiniciarclave2)).
      update((user.identificacion, user.usuario, user.nombre, user.email, user.fecharetiro, user.reiniciarclave1, 
          user.reiniciarclave2)).run
    user
  }

  //BORRAR
  def test(iduser: Int): models.DBReader[Seq[RolesRow]] = models.DBReader(0) { implicit s =>
    val res = for {
      rpu <- Rolporusuario if (rpu.idusuario === iduser)
      rol <- rpu.rolesFk
    } yield (rol)
    res.run
  }

}