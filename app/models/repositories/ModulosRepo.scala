package models.repositories

import models.Tables._
import models.domain.MySqlProfile

trait ModulosRepo extends BaseRepo[Int, ModulosRow] with MySqlProfile {
  import profile.simple._

  val query = Modulos

  def withId(model: ModulosRow, id: Int) = model.copy(id = Some(id))
  
  def modulosByUser(iduser: Int) = {
    for{
      rpu <- Rolporusuario if rpu.idusuario === iduser
      roles <- rpu.rolesFk
      ppm <- PermisosModulo if ppm.rol === roles.id
      modulo <- ppm.modulosFk
    } yield{
      modulo
    }
  }
  
  def getRolesxModule(module: String) =
    for{
      modulo <- Modulos if modulo.nombre.toLowerCase === module.toLowerCase
      ppm <- PermisosModulo if ppm.modulo === modulo.id
      rol <- ppm.rolesFk
    }yield{
      rol
    }
    
//  def getProcesosxModulo(module: String, iduser: Int) = {
//    val procesos = for {
//      proceso <- Procesos
//      modulo <- proceso.modulosFk if modulo.nombre.toLowerCase === module.toLowerCase
//    }yield (proceso)
//    
//    procesos
//  }
  
  def getProcesosxModuloUser(module: String, iduser: Int) = {
    for{
      rpu <- Rolporusuario if rpu.idusuario === iduser
      rol <- rpu.rolesFk
      ppr <- Permisosxrol if ppr.rol === rol.id
      proceso <- ppr.procesosFk
      modulo <- proceso.modulosFk if modulo.nombre.toLowerCase === module.toLowerCase
      tipoPerm <- ppr.tipoPermisoFk
    }yield (proceso, tipoPerm)
  }
  
 }