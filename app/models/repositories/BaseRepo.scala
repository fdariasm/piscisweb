package models.repositories

import scala.slick.driver.JdbcDriver.simple._
import models.domain.BaseEntity
import models.domain.TableWithId
import scala.Left
import scala.Right
import scalaz.Reader
 
/* Base repository for customized code generator
 * */
abstract class BaseRepo[I:BaseColumnType, T <: BaseEntity[I]] {
  val profile: scala.slick.driver.JdbcDriver
  import profile.simple._

  val query: TableQuery[_ <: Table[T] with TableWithId[I]] //with TableWithId[I]
   
  
  def withId(model: T, id: I): T

  def add(model: T): Reader[Session, T] = Reader (implicit s => {
    
    val id = query.returning(query.map(_.id)).insert(model)(s)
    withId(model, id)
  }
  )

  def update(model: T): Reader[Session, T] = Reader { implicit s =>
    val res = filterById(model.id.get).update(model).run
    model
  }

  def list: Reader[Session, List[T]] = Reader { implicit s => query.list }

  def pagedList(pageIndex: Int, limit: Int): Reader[Session, List[T]] = Reader { implicit s =>
    query.drop(pageIndex).take(limit).run.toList
  }

  def filterById(id: I) = query.filter(_.id === id)

  def deleteById(id: I): Reader[Session, Unit] = Reader { implicit s =>
    val res = filterById(id).delete
  }
  
  def delete(model: T): Reader[Session, Unit] = Reader { implicit s =>
    val res = filterById(model.id.get).delete
  }

  def findById(id: I): Reader[Session, Option[T]] = Reader { implicit s =>
    filterById(id).firstOption
  }
}
