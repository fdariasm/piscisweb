package models.repositories

import models.Tables._
import models.domain.MySqlProfile
import scalaz.Reader

trait ParametrosRepo extends BaseRepo[Int, ParametrosRow] with MySqlProfile {
  import profile.simple._
  
  val query = Parametros

  def withId(model: ParametrosRow, id: Int) = model.copy(id = Some(id))
}