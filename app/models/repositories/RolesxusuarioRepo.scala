package models.repositories

import models.Tables._
import models.domain.MySqlProfile
import scala.slick.driver.MySQLDriver.simple._

trait RolesxusuarioRepo extends BaseRepo[Int, RolporusuarioRow] with MySqlProfile {
  import profile.simple._

  val query = Rolporusuario

  def withId(model: RolporusuarioRow, id: Int) = model.copy(id = Some(id))

  def rolesUsuario(iduser: Int) = {
    for {
      rpu <- query if rpu.idusuario === iduser
      rol <- rpu.rolesFk
    } yield (rpu, rol)
  }

  def findByRol(iduser: Int, idrol: Int) = {
    query.filter(r => r.idrol === idrol && r.idusuario === iduser)
  }
}