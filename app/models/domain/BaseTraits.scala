package models.domain

import scala.slick.driver.JdbcDriver.simple._

trait BaseEntity[I] extends Product{
  val id: Option[I]
}

trait TableWithId[I] {
  val id: Column[I]
}

trait BaseTables {
  val profile: scala.slick.driver.JdbcProfile
  import profile.simple._
}