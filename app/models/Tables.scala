package models
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = scala.slick.driver.MySQLDriver
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: scala.slick.driver.JdbcProfile
  import profile.simple._
  import scala.slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import scala.slick.jdbc.{GetResult => GR}
  
  /** DDL for all tables. Call .create to execute. */
  lazy val ddl = CamposRegistro.ddl ++ Modulos.ddl ++ Parametros.ddl ++ PermisosModulo.ddl ++ Permisosxrol.ddl ++ Procesos.ddl ++ RegistroActividades.ddl ++ Roles.ddl ++ Rolporusuario.ddl ++ TipoPermiso.ddl ++ Usuarios.ddl
  
  /** Entity class storing rows of table CamposRegistro
   *  @param nombre Database column nombre DBType(VARCHAR), Length(80,true)
   *  @param valor Database column valor DBType(TEXT), Length(65535,true)
   *  @param valorAnterior Database column valor_anterior DBType(TEXT), Length(65535,true), Default(None)
   *  @param registroActividades Database column registro_actividades DBType(INT)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class CamposRegistroRow(nombre: String, valor: String, valorAnterior: Option[String] = None, registroActividades: Int, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching CamposRegistroRow objects using plain SQL queries */
  implicit def GetResultCamposRegistroRow(implicit e0: GR[String], e1: GR[Option[String]], e2: GR[Int], e3: GR[Option[Int]]): GR[CamposRegistroRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[String], <<[String], <<?[String], <<[Int])
    import r._
    CamposRegistroRow.tupled((_2, _3, _4, _5, _1)) // putting AutoInc last
  }
  /** Table description of table campos_registro. Objects of this class serve as prototypes for rows in queries. */
  class CamposRegistro(_tableTag: Tag) extends Table[CamposRegistroRow](_tableTag, "campos_registro") with models.domain.TableWithId[Int] {
    def * = (nombre, valor, valorAnterior, registroActividades, id.?) <> (CamposRegistroRow.tupled, CamposRegistroRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (nombre.?, valor.?, valorAnterior, registroActividades.?, id.?).shaped.<>({r=>import r._; _1.map(_=> CamposRegistroRow.tupled((_1.get, _2.get, _3, _4.get, _5)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column nombre DBType(VARCHAR), Length(80,true) */
    val nombre: Column[String] = column[String]("nombre", O.Length(80,varying=true))
    /** Database column valor DBType(TEXT), Length(65535,true) */
    val valor: Column[String] = column[String]("valor", O.Length(65535,varying=true))
    /** Database column valor_anterior DBType(TEXT), Length(65535,true), Default(None) */
    val valorAnterior: Column[Option[String]] = column[Option[String]]("valor_anterior", O.Length(65535,varying=true), O.Default(None))
    /** Database column registro_actividades DBType(INT) */
    val registroActividades: Column[Int] = column[Int]("registro_actividades")
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    
    /** Foreign key referencing RegistroActividades (database name fk_table1_registro_actividades1) */
    lazy val registroActividadesFk = foreignKey("fk_table1_registro_actividades1", registroActividades, RegistroActividades)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table CamposRegistro */
  lazy val CamposRegistro = new TableQuery(tag => new CamposRegistro(tag))
  
  /** Entity class storing rows of table Modulos
   *  @param  nombre Database column  nombre DBType(VARCHAR), Length(45,true)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class ModulosRow( nombre: String, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching ModulosRow objects using plain SQL queries */
  implicit def GetResultModulosRow(implicit e0: GR[String], e1: GR[Option[Int]]): GR[ModulosRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[String])
    import r._
    ModulosRow.tupled((_2, _1)) // putting AutoInc last
  }
  /** Table description of table modulos. Objects of this class serve as prototypes for rows in queries. */
  class Modulos(_tableTag: Tag) extends Table[ModulosRow](_tableTag, "modulos") with models.domain.TableWithId[Int] {
    def * = ( nombre, id.?) <> (ModulosRow.tupled, ModulosRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ( nombre.?, id.?).shaped.<>({r=>import r._; _1.map(_=> ModulosRow.tupled((_1.get, _2)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column  nombre DBType(VARCHAR), Length(45,true) */
    val  nombre: Column[String] = column[String](" nombre", O.Length(45,varying=true))
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    
    /** Uniqueness Index over ( nombre) (database name  nombre_UNIQUE) */
    val index1 = index(" nombre_UNIQUE",  nombre, unique=true)
  }
  /** Collection-like TableQuery object for table Modulos */
  lazy val Modulos = new TableQuery(tag => new Modulos(tag))
  
  /** Entity class storing rows of table Parametros
   *  @param servidor Database column servidor DBType(VARCHAR), Length(200,true)
   *  @param protocolo Database column protocolo DBType(VARCHAR), Length(10,true), Default(None)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class ParametrosRow(servidor: String, protocolo: Option[String] = None, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching ParametrosRow objects using plain SQL queries */
  implicit def GetResultParametrosRow(implicit e0: GR[String], e1: GR[Option[String]], e2: GR[Option[Int]]): GR[ParametrosRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[String], <<?[String])
    import r._
    ParametrosRow.tupled((_2, _3, _1)) // putting AutoInc last
  }
  /** Table description of table parametros. Objects of this class serve as prototypes for rows in queries. */
  class Parametros(_tableTag: Tag) extends Table[ParametrosRow](_tableTag, "parametros") with models.domain.TableWithId[Int] {
    def * = (servidor, protocolo, id.?) <> (ParametrosRow.tupled, ParametrosRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (servidor.?, protocolo, id.?).shaped.<>({r=>import r._; _1.map(_=> ParametrosRow.tupled((_1.get, _2, _3)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column servidor DBType(VARCHAR), Length(200,true) */
    val servidor: Column[String] = column[String]("servidor", O.Length(200,varying=true))
    /** Database column protocolo DBType(VARCHAR), Length(10,true), Default(None) */
    val protocolo: Column[Option[String]] = column[Option[String]]("protocolo", O.Length(10,varying=true), O.Default(None))
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
  }
  /** Collection-like TableQuery object for table Parametros */
  lazy val Parametros = new TableQuery(tag => new Parametros(tag))
  
  /** Entity class storing rows of table PermisosModulo
   *  @param rol Database column rol DBType(INT)
   *  @param modulo Database column modulo DBType(INT)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class PermisosModuloRow(rol: Int, modulo: Int, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching PermisosModuloRow objects using plain SQL queries */
  implicit def GetResultPermisosModuloRow(implicit e0: GR[Int], e1: GR[Option[Int]]): GR[PermisosModuloRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[Int], <<[Int])
    import r._
    PermisosModuloRow.tupled((_2, _3, _1)) // putting AutoInc last
  }
  /** Table description of table permisos_modulo. Objects of this class serve as prototypes for rows in queries. */
  class PermisosModulo(_tableTag: Tag) extends Table[PermisosModuloRow](_tableTag, "permisos_modulo") with models.domain.TableWithId[Int] {
    def * = (rol, modulo, id.?) <> (PermisosModuloRow.tupled, PermisosModuloRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (rol.?, modulo.?, id.?).shaped.<>({r=>import r._; _1.map(_=> PermisosModuloRow.tupled((_1.get, _2.get, _3)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column rol DBType(INT) */
    val rol: Column[Int] = column[Int]("rol")
    /** Database column modulo DBType(INT) */
    val modulo: Column[Int] = column[Int]("modulo")
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    
    /** Foreign key referencing Modulos (database name fk_permisos_modulo_modulos1) */
    lazy val modulosFk = foreignKey("fk_permisos_modulo_modulos1", modulo, Modulos)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Roles (database name fk_permisos_modulo_roles1) */
    lazy val rolesFk = foreignKey("fk_permisos_modulo_roles1", rol, Roles)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table PermisosModulo */
  lazy val PermisosModulo = new TableQuery(tag => new PermisosModulo(tag))
  
  /** Entity class storing rows of table Permisosxrol
   *  @param tipoPermiso Database column tipo_permiso DBType(INT)
   *  @param procesos Database column procesos DBType(INT)
   *  @param rol Database column rol DBType(INT)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class PermisosxrolRow(tipoPermiso: Int, procesos: Int, rol: Int, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching PermisosxrolRow objects using plain SQL queries */
  implicit def GetResultPermisosxrolRow(implicit e0: GR[Int], e1: GR[Option[Int]]): GR[PermisosxrolRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[Int], <<[Int], <<[Int])
    import r._
    PermisosxrolRow.tupled((_2, _3, _4, _1)) // putting AutoInc last
  }
  /** Table description of table permisosxrol. Objects of this class serve as prototypes for rows in queries. */
  class Permisosxrol(_tableTag: Tag) extends Table[PermisosxrolRow](_tableTag, "permisosxrol") with models.domain.TableWithId[Int] {
    def * = (tipoPermiso, procesos, rol, id.?) <> (PermisosxrolRow.tupled, PermisosxrolRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (tipoPermiso.?, procesos.?, rol.?, id.?).shaped.<>({r=>import r._; _1.map(_=> PermisosxrolRow.tupled((_1.get, _2.get, _3.get, _4)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column tipo_permiso DBType(INT) */
    val tipoPermiso: Column[Int] = column[Int]("tipo_permiso")
    /** Database column procesos DBType(INT) */
    val procesos: Column[Int] = column[Int]("procesos")
    /** Database column rol DBType(INT) */
    val rol: Column[Int] = column[Int]("rol")
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    
    /** Foreign key referencing Procesos (database name fk_permisosxrol_procesos1) */
    lazy val procesosFk = foreignKey("fk_permisosxrol_procesos1", procesos, Procesos)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Roles (database name fk_permisosxrol_roles1) */
    lazy val rolesFk = foreignKey("fk_permisosxrol_roles1", rol, Roles)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing TipoPermiso (database name fk_permisosxrol_tipo_permiso1) */
    lazy val tipoPermisoFk = foreignKey("fk_permisosxrol_tipo_permiso1", tipoPermiso, TipoPermiso)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    
    /** Uniqueness Index over (procesos,tipoPermiso,rol) (database name procesos_UNIQUE) */
    val index1 = index("procesos_UNIQUE", (procesos, tipoPermiso, rol), unique=true)
    /** Uniqueness Index over (tipoPermiso,procesos,rol) (database name tipo_permiso_UNIQUE) */
    val index2 = index("tipo_permiso_UNIQUE", (tipoPermiso, procesos, rol), unique=true)
  }
  /** Collection-like TableQuery object for table Permisosxrol */
  lazy val Permisosxrol = new TableQuery(tag => new Permisosxrol(tag))
  
  /** Entity class storing rows of table Procesos
   *  @param nombre Database column nombre DBType(VARCHAR), Length(45,true)
   *  @param descripcion Database column descripcion DBType(VARCHAR), Length(100,true), Default(None)
   *  @param modulo Database column modulo DBType(INT)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class ProcesosRow(nombre: String, descripcion: Option[String] = None, modulo: Int, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching ProcesosRow objects using plain SQL queries */
  implicit def GetResultProcesosRow(implicit e0: GR[String], e1: GR[Option[String]], e2: GR[Int], e3: GR[Option[Int]]): GR[ProcesosRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[String], <<?[String], <<[Int])
    import r._
    ProcesosRow.tupled((_2, _3, _4, _1)) // putting AutoInc last
  }
  /** Table description of table procesos. Objects of this class serve as prototypes for rows in queries. */
  class Procesos(_tableTag: Tag) extends Table[ProcesosRow](_tableTag, "procesos") with models.domain.TableWithId[Int] {
    def * = (nombre, descripcion, modulo, id.?) <> (ProcesosRow.tupled, ProcesosRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (nombre.?, descripcion, modulo.?, id.?).shaped.<>({r=>import r._; _1.map(_=> ProcesosRow.tupled((_1.get, _2, _3.get, _4)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column nombre DBType(VARCHAR), Length(45,true) */
    val nombre: Column[String] = column[String]("nombre", O.Length(45,varying=true))
    /** Database column descripcion DBType(VARCHAR), Length(100,true), Default(None) */
    val descripcion: Column[Option[String]] = column[Option[String]]("descripcion", O.Length(100,varying=true), O.Default(None))
    /** Database column modulo DBType(INT) */
    val modulo: Column[Int] = column[Int]("modulo")
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    
    /** Foreign key referencing Modulos (database name fk_table1_modulos1) */
    lazy val modulosFk = foreignKey("fk_table1_modulos1", modulo, Modulos)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    
    /** Uniqueness Index over (nombre) (database name nombre_UNIQUE) */
    val index1 = index("nombre_UNIQUE", nombre, unique=true)
  }
  /** Collection-like TableQuery object for table Procesos */
  lazy val Procesos = new TableQuery(tag => new Procesos(tag))
  
  /** Entity class storing rows of table RegistroActividades
   *  @param fecha Database column fecha DBType(DATETIME)
   *  @param accion Database column accion DBType(CHAR), Length(1,false)
   *  @param proceso Database column proceso DBType(VARCHAR), Length(60,true)
   *  @param usuario Database column usuario DBType(INT)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class RegistroActividadesRow(fecha: java.sql.Timestamp, accion: String, proceso: String, usuario: Int, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching RegistroActividadesRow objects using plain SQL queries */
  implicit def GetResultRegistroActividadesRow(implicit e0: GR[java.sql.Timestamp], e1: GR[String], e2: GR[Int], e3: GR[Option[Int]]): GR[RegistroActividadesRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[java.sql.Timestamp], <<[String], <<[String], <<[Int])
    import r._
    RegistroActividadesRow.tupled((_2, _3, _4, _5, _1)) // putting AutoInc last
  }
  /** Table description of table registro_actividades. Objects of this class serve as prototypes for rows in queries. */
  class RegistroActividades(_tableTag: Tag) extends Table[RegistroActividadesRow](_tableTag, "registro_actividades") with models.domain.TableWithId[Int] {
    def * = (fecha, accion, proceso, usuario, id.?) <> (RegistroActividadesRow.tupled, RegistroActividadesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (fecha.?, accion.?, proceso.?, usuario.?, id.?).shaped.<>({r=>import r._; _1.map(_=> RegistroActividadesRow.tupled((_1.get, _2.get, _3.get, _4.get, _5)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column fecha DBType(DATETIME) */
    val fecha: Column[java.sql.Timestamp] = column[java.sql.Timestamp]("fecha")
    /** Database column accion DBType(CHAR), Length(1,false) */
    val accion: Column[String] = column[String]("accion", O.Length(1,varying=false))
    /** Database column proceso DBType(VARCHAR), Length(60,true) */
    val proceso: Column[String] = column[String]("proceso", O.Length(60,varying=true))
    /** Database column usuario DBType(INT) */
    val usuario: Column[Int] = column[Int]("usuario")
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    
    /** Foreign key referencing Usuarios (database name fk_registro_actividades_usuarios1) */
    lazy val usuariosFk = foreignKey("fk_registro_actividades_usuarios1", usuario, Usuarios)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table RegistroActividades */
  lazy val RegistroActividades = new TableQuery(tag => new RegistroActividades(tag))
  
  /** Entity class storing rows of table Roles
   *  @param nombre Database column nombre DBType(VARCHAR), Length(60,true)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class RolesRow(nombre: String, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching RolesRow objects using plain SQL queries */
  implicit def GetResultRolesRow(implicit e0: GR[String], e1: GR[Option[Int]]): GR[RolesRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[String])
    import r._
    RolesRow.tupled((_2, _1)) // putting AutoInc last
  }
  /** Table description of table roles. Objects of this class serve as prototypes for rows in queries. */
  class Roles(_tableTag: Tag) extends Table[RolesRow](_tableTag, "roles") with models.domain.TableWithId[Int] {
    def * = (nombre, id.?) <> (RolesRow.tupled, RolesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (nombre.?, id.?).shaped.<>({r=>import r._; _1.map(_=> RolesRow.tupled((_1.get, _2)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column nombre DBType(VARCHAR), Length(60,true) */
    val nombre: Column[String] = column[String]("nombre", O.Length(60,varying=true))
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
  }
  /** Collection-like TableQuery object for table Roles */
  lazy val Roles = new TableQuery(tag => new Roles(tag))
  
  /** Entity class storing rows of table Rolporusuario
   *  @param idrol Database column idrol DBType(INT)
   *  @param idusuario Database column idusuario DBType(INT)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class RolporusuarioRow(idrol: Int, idusuario: Int, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching RolporusuarioRow objects using plain SQL queries */
  implicit def GetResultRolporusuarioRow(implicit e0: GR[Int], e1: GR[Option[Int]]): GR[RolporusuarioRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[Int], <<[Int])
    import r._
    RolporusuarioRow.tupled((_2, _3, _1)) // putting AutoInc last
  }
  /** Table description of table rolporusuario. Objects of this class serve as prototypes for rows in queries. */
  class Rolporusuario(_tableTag: Tag) extends Table[RolporusuarioRow](_tableTag, "rolporusuario") with models.domain.TableWithId[Int] {
    def * = (idrol, idusuario, id.?) <> (RolporusuarioRow.tupled, RolporusuarioRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (idrol.?, idusuario.?, id.?).shaped.<>({r=>import r._; _1.map(_=> RolporusuarioRow.tupled((_1.get, _2.get, _3)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column idrol DBType(INT) */
    val idrol: Column[Int] = column[Int]("idrol")
    /** Database column idusuario DBType(INT) */
    val idusuario: Column[Int] = column[Int]("idusuario")
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    
    /** Foreign key referencing Roles (database name fk_rolporusuario_roles) */
    lazy val rolesFk = foreignKey("fk_rolporusuario_roles", idrol, Roles)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Usuarios (database name fk_rolporusuario_usuarios1) */
    lazy val usuariosFk = foreignKey("fk_rolporusuario_usuarios1", idusuario, Usuarios)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Rolporusuario */
  lazy val Rolporusuario = new TableQuery(tag => new Rolporusuario(tag))
  
  /** Entity class storing rows of table TipoPermiso
   *  @param nombre Database column nombre DBType(VARCHAR), Length(45,true)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class TipoPermisoRow(nombre: String, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching TipoPermisoRow objects using plain SQL queries */
  implicit def GetResultTipoPermisoRow(implicit e0: GR[String], e1: GR[Option[Int]]): GR[TipoPermisoRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[String])
    import r._
    TipoPermisoRow.tupled((_2, _1)) // putting AutoInc last
  }
  /** Table description of table tipo_permiso. Objects of this class serve as prototypes for rows in queries. */
  class TipoPermiso(_tableTag: Tag) extends Table[TipoPermisoRow](_tableTag, "tipo_permiso") with models.domain.TableWithId[Int] {
    def * = (nombre, id.?) <> (TipoPermisoRow.tupled, TipoPermisoRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (nombre.?, id.?).shaped.<>({r=>import r._; _1.map(_=> TipoPermisoRow.tupled((_1.get, _2)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column nombre DBType(VARCHAR), Length(45,true) */
    val nombre: Column[String] = column[String]("nombre", O.Length(45,varying=true))
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
  }
  /** Collection-like TableQuery object for table TipoPermiso */
  lazy val TipoPermiso = new TableQuery(tag => new TipoPermiso(tag))
  
  /** Entity class storing rows of table Usuarios
   *  @param identificacion Database column identificacion DBType(VARCHAR), Length(45,true)
   *  @param usuario Database column usuario DBType(VARCHAR), Length(25,true)
   *  @param nombre Database column nombre DBType(VARCHAR), Length(100,true)
   *  @param email Database column email DBType(VARCHAR), Length(200,true)
   *  @param clave1 Database column clave1 DBType(VARCHAR), Length(300,true)
   *  @param clave2 Database column clave2 DBType(VARCHAR), Length(300,true)
   *  @param fecharetiro Database column fecharetiro DBType(DATETIME), Default(None)
   *  @param reiniciarclave1 Database column reiniciarclave1 DBType(BIT), Default(false)
   *  @param reiniciarclave2 Database column reiniciarclave2 DBType(BIT), Default(false)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class UsuariosRow(identificacion: String, usuario: String, nombre: String, email: String, clave1: String, clave2: String, fecharetiro: Option[java.sql.Timestamp] = None, reiniciarclave1: Boolean = false, reiniciarclave2: Boolean = false, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching UsuariosRow objects using plain SQL queries */
  implicit def GetResultUsuariosRow(implicit e0: GR[String], e1: GR[Option[java.sql.Timestamp]], e2: GR[Boolean], e3: GR[Option[Int]]): GR[UsuariosRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[String], <<[String], <<[String], <<[String], <<[String], <<[String], <<?[java.sql.Timestamp], <<[Boolean], <<[Boolean])
    import r._
    UsuariosRow.tupled((_2, _3, _4, _5, _6, _7, _8, _9, _10, _1)) // putting AutoInc last
  }
  /** Table description of table usuarios. Objects of this class serve as prototypes for rows in queries. */
  class Usuarios(_tableTag: Tag) extends Table[UsuariosRow](_tableTag, "usuarios") with models.domain.TableWithId[Int] {
    def * = (identificacion, usuario, nombre, email, clave1, clave2, fecharetiro, reiniciarclave1, reiniciarclave2, id.?) <> (UsuariosRow.tupled, UsuariosRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (identificacion.?, usuario.?, nombre.?, email.?, clave1.?, clave2.?, fecharetiro, reiniciarclave1.?, reiniciarclave2.?, id.?).shaped.<>({r=>import r._; _1.map(_=> UsuariosRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7, _8.get, _9.get, _10)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column identificacion DBType(VARCHAR), Length(45,true) */
    val identificacion: Column[String] = column[String]("identificacion", O.Length(45,varying=true))
    /** Database column usuario DBType(VARCHAR), Length(25,true) */
    val usuario: Column[String] = column[String]("usuario", O.Length(25,varying=true))
    /** Database column nombre DBType(VARCHAR), Length(100,true) */
    val nombre: Column[String] = column[String]("nombre", O.Length(100,varying=true))
    /** Database column email DBType(VARCHAR), Length(200,true) */
    val email: Column[String] = column[String]("email", O.Length(200,varying=true))
    /** Database column clave1 DBType(VARCHAR), Length(300,true) */
    val clave1: Column[String] = column[String]("clave1", O.Length(300,varying=true))
    /** Database column clave2 DBType(VARCHAR), Length(300,true) */
    val clave2: Column[String] = column[String]("clave2", O.Length(300,varying=true))
    /** Database column fecharetiro DBType(DATETIME), Default(None) */
    val fecharetiro: Column[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("fecharetiro", O.Default(None))
    /** Database column reiniciarclave1 DBType(BIT), Default(false) */
    val reiniciarclave1: Column[Boolean] = column[Boolean]("reiniciarclave1", O.Default(false))
    /** Database column reiniciarclave2 DBType(BIT), Default(false) */
    val reiniciarclave2: Column[Boolean] = column[Boolean]("reiniciarclave2", O.Default(false))
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    
    /** Uniqueness Index over (identificacion) (database name identificacion_UNIQUE) */
    val index1 = index("identificacion_UNIQUE", identificacion, unique=true)
  }
  /** Collection-like TableQuery object for table Usuarios */
  lazy val Usuarios = new TableQuery(tag => new Usuarios(tag))
}