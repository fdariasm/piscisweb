function getAuthService() {
	var AuthService = function($http) {
		var procesos = {};
		
		this.setPermisos = function(loadedProcesos){
			procesos = loadedProcesos;
		};
		
		this.tienePermiso = function(proceso, permiso){
			if(!(proceso in procesos)) return false;
			var permisos = procesos[proceso];
			for (i = 0; i < permisos.length; i++) {
				var ipermiso = permisos[i];
				if ( ipermiso === permiso ) {
					return true;
				}
			}
			return false;
		};
	};
	AuthService.$inject = ['$http'];
	return AuthService;
	//angular.module('playAppointments').service('usersService', usersService);
}