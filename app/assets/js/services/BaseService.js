var BaseService = function(url){
	this.url = url;
	this.get = function(){
		return this.$http.get(this.url);
	};
	
	this.save = function(model){
		return this.$http.post(this.url, model);
	};
	this.delete =  function(id){
		return this.$http.delete(this.url + "/" + id );
	};
	this.update = function(id, model){		
		return this.$http.put(this.url + "/" + id, model);
	};
};

