var RolesService = function($http){
	this.$http = $http;
	
	this.$inject = ['$http'];
	
	this.getRolesUsuario = function(iduser){
		return this.$http.get('/api/usuarios/' + iduser + '/roles');
	};
	
	this.agregarRol = function(iduser, idrol) {
		return this.$http.post('/api/usuarios/' + iduser + '/roles', angular.toJson(idrol));
	};
	
	this.eliminarRolusuario = function(idrolusuario) {
		return this.$http.delete('/api/rolesusuario/' + idrolusuario);
	};
	
	this.getRolesModulo = function(idmodulo){
		return this.$http.get('/api/modulos/' + idmodulo + '/roles');
	};
};

RolesService.prototype = new BaseService("/api/roles");