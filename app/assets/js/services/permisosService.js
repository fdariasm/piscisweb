function getPermService($http, vistas) {
	var PermisosService = function() {
		var tienePerm = function(procesos) {
			return $http.post('/api/permisos', procesos);
		};

		var permisos = {};
		function getPermisos() {
			tienePerm(vistas).success(function(p) {
				permisos = p;
			}).error(function(data, status, header, config) {
				console.log("Error: " + data);
			});
		}
		getPermisos();

		this.tienePermiso = function(proceso) {
			for (i = 0; i < permisos.length; i++) {
				var iproceso = permisos[i];
				if (iproceso.proceso === proceso && iproceso.permiso) {
					return true;
				}
			}
			return false;
		};
		
		var perm = {
				nombre: "Configuraciones",
				permisos: {
					ingresar: true,
					insertar: true,
					actualizar: false,
					eliminar: false
				}
		};
	};

	return new PermisosService();
}