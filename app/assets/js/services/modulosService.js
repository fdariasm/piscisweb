var modulosService = function($http){
	this.$http =$http;
	
	this.$inject = ['$http'];
	
	this.agregarRol = function(idmodulo, idrol) {
		return this.$http.post('/api/modulos/' + idmodulo + '/roles', angular.toJson(idrol));
	};

	this.eliminarRol = function(idrolModulo) {
		return this.$http.delete('/api/rolesmodulo/' + idrolModulo);
	};
};

modulosService.prototype = new BaseService("/api/modulos");