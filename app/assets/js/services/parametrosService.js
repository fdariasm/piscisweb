function getParametrosService() {
	var paramService = function($http) {
		this.getParams = function(){
			return $http.get('/api/parametros');
		};
		
		this.saveParam = function(param){
			return $http.post('/api/parametros', param);
		};
		
		this.updateParam = function(id){
			return function(param){
				return $http.put('/api/parametros/' + id, param);
			};
		};
		this.deleteParam = function(id){
			return $http.delete('/api/parametros/' + id);
		};
	};
	paramService.$inject = ['$http'];
	return paramService;
	//angular.module('playAppointments').service('usersService', usersService);
}