function getRolesController( ){
	var rolesController = function($scope, rolesService, $modal){
		$scope.roles = [];
		
		function getRoles(){
			rolesService.get().success(function(data){
				$scope.roles = data.response;
			}).error(function(data, status, header, config) {
				console.log("Error: " + data);
			});
		}
		
		function openModal(saver, rol) {
			var modalInstance = $modal.open({
				templateUrl : 'editRol.html',
				controller : 'ModalController',
				resolve: {
					model: function () {
						return angular.copy( rol );
					},
					options: {}
				}
			});

			modalInstance.result.then(function(rol) {
				saver(rol).success(function() {
					getRoles();
				}).error(function(data, status, header, config) {
					console.log("Error: " + data);
				});
			});
		}
		
		$scope.newRol = function() {
			openModal(function(rol){ return rolesService.save(rol); }, {});
		};

		$scope.updateRol = function(id, rolin) {
			openModal(function(newrol){
				return rolesService.update(id, newrol);
			}, rolin);
		};

		$scope.deleteRol = function(id) {
			rolesService.delete(id).success(function() {
				getRoles();
			}).error(function(data, status, header, config) {
				console.log("Error: " + data);
			});
		};
		getRoles();
	};
	
	rolesController.$inject = [ '$scope', 'rolesService', "$modal" ];
	return rolesController;
}