function getModalController(){
	var controller = function($scope, $modalInstance, model, options) {
		$scope.model = model;
		$scope.options = options;
		$scope.save = function(model) {
			$modalInstance.close(model);
		};
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
	};
	controller.$inject = [ "$scope", "$modalInstance", "model", "options" ];
	return controller;
}