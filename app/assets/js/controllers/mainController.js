(function(){
	var Controller = function($scope){
		$scope.greeting = "Hello Piscis";
	};
	
	Controller.$inject = ["$scope"];
	angular.module("piscisMain").controller("mainController",
			Controller);
})();