function getRolesUsuarioController(){
	var controller = function($scope, $modalInstance, usuario, rolesService, growl){
		$scope.usuario = usuario;
		$scope.rolesUsuario = [];
		$scope.roles = [];
		$scope.selected = {};
		
		function getRolesUsuario(){
			rolesService.getRolesUsuario(usuario.id).success(function(data){
				$scope.rolesUsuario = data.response;
			});
		}
		getRolesUsuario();
		
		function getRoles(){
			rolesService.get().success(function(roles){
				$scope.roles = roles.response;
			});
		}
		getRoles();
		
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
		
		$scope.asignarRol = function(){
			if(!$scope.selected.rol) {
				growl.error("Debe seleccionar un rol");
				return;
			}
			var idrol = $scope.selected.rol.id;
			rolesService.agregarRol(usuario.id, idrol).success(function(response){
				getRolesUsuario();
				growl.success("Rol asignado correctamente", {ttl: 3000});
			}).error(function(data, status, header, config) {
				console.log("Error: ");
				console.log( data );
				if(data.message)
					growl.error(data.message, {ttl: 5000});
			});
		};
		
		$scope.eliminarRol = function(idroluser) {
			rolesService.eliminarRolusuario(idroluser).success(function(data){
				getRolesUsuario();
				growl.success(data.respobse);
			}).error(function(data, status, header, config) {
				console.log("Error: ");
				console.log( data );
				if(data.errors)
					growl.error(data.errors[0].friendlyMessage, {ttl: 5000});
			});
		} ;
	};
	
	controller.$inject = [ "$scope", "$modalInstance", "usuario", "rolesService", 'growl'];
	return controller;
}