(function(){
	var Controller = function($scope, $http, authService){
		
		$scope.tienePermiso = function(proceso, permiso){
			return authService.tienePermiso(proceso, permiso);
		};
		
		
	};
	
	Controller.$inject = ["$scope", "$http", "authService"];
	angular.module("piscisConfig").controller("configController",
			Controller);
})();