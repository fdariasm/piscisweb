(function() {
	var Controller = function($scope, usersService, $modal, authService) {
		$scope.users = [];

		function getUsers() {
			usersService.get().success(function(data) {
				$scope.users = data.response;
			}).error(function(data, status, header, config) {
				console.log("Error: ");
				console.log( data );
			});
		}		
		$scope.newUser = function() {
			openModal(function(user){
				user.reiniciarclave1 = true;
				user.reiniciarclave2 = true;
				return usersService.save(user);
			}, {});
		};

		$scope.updateUser = function(id, userin) {
			userin.fecharetiroDate = new Date(userin.fecharetiro);
			openModal(function(user){
				return usersService.update(id, user);
			}, userin);
		};

		$scope.deleteUser = function(id) {
			usersService.delete(id).success(function(users) {
				getUsers();
			}).error(function(data, status, header, config) {
				console.log("Error: " + data);
			});
		};
		getUsers();
		
		$scope.tienePermiso = authService.tienePermiso;

		function openModal(saver, model) {
			var modalInstance = $modal.open({
				templateUrl : 'editUser.html',
				controller : 'GModalCtrl',
				resolve: {
					model: function () {
						return angular.copy(model);
					}
				}
			});

			modalInstance.result.then(function(user) {
				user.fecharetiro = formatDate(user.fecharetiro); //manual filter
				saver(user).success(function() {
					getUsers();
				}).error(function(data, status, header, config) {
					console.log("Error: ");
					console.log(data);
				});
			});
		}
		
		$scope.mostrarRoles = function(usuario) {
			var modalInstance = $modal.open({
				templateUrl : 'verRoles.html',
				controller : 'rolesUsuarioController',
				resolve: {
					usuario : function(){ return usuario;}
				}
			});
			modalInstance.result.then(function(asdf){
				
			});
		};
		
		function formatDate(d){
			var curr_date = d.getDate();
			var curr_month = d.getMonth() + 1; //Months are zero based
			var curr_year = d.getFullYear();
			return (curr_year + "-" + curr_month + "-" + curr_date);
		}
	};

	Controller.$inject = [ '$scope', 'usersService', "$modal", "authService" ];
	angular.module("piscisConfig").controller("usersController",
			Controller);

	var GModalCtrl = function($scope, $modalInstance, model) {
		$scope.model = model;
		
		$scope.save = function(model) {
			$modalInstance.close(model);
		};

		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
		
		$scope.open = function($event) {
				$event.preventDefault();
				$event.stopPropagation();
				$scope.opened = true;
		};
		
		$scope.dateOptions = {
				formatYear: 'yy',
				startingDay: 1
		};
		$scope.format = 'dd-MMMM-yyyy';
	};
	GModalCtrl.$inject = [ "$scope", "$modalInstance", "model"];
	angular.module("piscisConfig").controller("GModalCtrl", GModalCtrl);
	
})();