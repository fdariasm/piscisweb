(function() {
	var Controller = function($scope, usersService) {
		$scope.greeting = usersService.test();
	};

	Controller.$inject = [ '$scope', 'usersService' ];
	angular.module("piscisConfig").controller("testController",
			Controller);
})();