(function() {
	var Controller = function($scope, paramService, $modal) {
		$scope.params = [];

		function getParams() {
			paramService.getParams().success(function(data) {
				$scope.params = data.response;
			}).error(function(data, status, header, config) {
				console.log("Error: " + data);
			});
		}

		$scope.newParam = function() {
			openModal(paramService.saveParam, {});

		};

		$scope.updateParam = function(id, param) {
			openModal(paramService.updateParam(id), param);
		};

		$scope.deleteParam = function(id) {
			paramService.deleteParam(id).success(function(params) {
				getParams();
			}).error(function(data, status, header, config) {
				console.log("Error: " + data);
			});
		};
		getParams();

		function openModal(saver, param) {
			var modalInstance = $modal.open({
				templateUrl : 'editParametro.html',
				controller : 'ModalCtrl',
				resolve: {
					param: function () {
						return angular.copy(param);
					}
				}
			});

			modalInstance.result.then(function(param) {
				saver(param).success(function() {
					getParams();
				}).error(function(data, status, header, config) {
					console.log("Error: " + data);
				});
			});
		}
	};

	Controller.$inject = [ '$scope', 'paramsService', "$modal" ];
	angular.module("piscisConfig").controller("parametrosController",
			Controller);

	var ModalCtrl = function($scope, $modalInstance, param) {
		$scope.param = param;
		$scope.saveParam = function(param) {
			$modalInstance.close(param);
		};

		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
	};
	ModalCtrl.$inject = [ "$scope", "$modalInstance", "param"];
	angular.module("piscisConfig").controller("ModalCtrl", ModalCtrl);
})();