function getModulosController(){
	var modulosController = function($scope, modulosService, $modal){
		$scope.modulos = [];
		
		function getModulos(){
			modulosService.get().success(function(data){
				$scope.modulos = data.response;
			}).error(function(data, status, header, config) {
				console.log("Error: " + data);
			});
		}
		getModulos();
		
		$scope.mostrarRoles = function(modulo) {
			var modalInstance = $modal.open({
				templateUrl : 'verRolesModulo.html',
				controller : 'rolesModuloController',
				resolve: {
					modulo : function(){ return modulo;}
				}
			});
			modalInstance.result.then(function(asdf){
				
			});
		};
	};
	modulosController.$inject = [ '$scope', 'modulosService', '$modal' ];
	return modulosController;
}