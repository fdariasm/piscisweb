function getRolesModuloController(){
	var controller = function($scope, $modalInstance, modulo, modulosService, rolesService, growl){
		$scope.modulo = modulo;
		$scope.rolesModulo = [];
		$scope.roles = [];
		$scope.selected = {};
		
		function getRolesModulo(){
			rolesService.getRolesModulo(modulo.id).success(function(data){
				$scope.rolesModulo = data.response;
			});
		}
		getRolesModulo();
		
		function getRoles(){
			rolesService.get().success(function(data){
				$scope.roles = data.response;
			});
		}
		getRoles();
		
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
		
		$scope.asignarRol = function(){
			if(!$scope.selected.rol) {
				growl.error("Debe seleccionar un rol");
				return;
			}
			var idrol = $scope.selected.rol.id;
			modulosService.agregarRol(modulo.id, idrol).success(function(data){
				getRolesModulo();
				growl.success('Rol asignado correctamente', {ttl: 3000});
			}).error(function(data, status, header, config) {
				console.log("Error: ");
				console.log( data );
				if(data.errors)
					growl.error(data.errors[0].friendlyMessage, {ttl: 5000});
			});
		};
		
		$scope.eliminarRol = function(idrolmodulo) {
			modulosService.eliminarRol(idrolmodulo).success(function(data){
				getRolesModulo();
				growl.success(data.response);
			}).error(function(data, status, header, config) {
				console.log("Error: ");
				console.log( data );
				if(data.errors)
					growl.error(data.errors[0].friendlyMessage, {ttl: 5000});
			});
		};
	};
	
	controller.$inject = [ "$scope", "$modalInstance", "modulo","modulosService", "rolesService", 'growl'];
	return controller;
}