(function() {
	var app = angular.module('piscisMain', [ 'ngRoute','ngMessages']);
	app.config(function($routeProvider) {
		$routeProvider.when('/', {
			controller : "mainController",
			templateUrl : '/vassets/views/main.html'
		});
	});
	
	angular.bootstrap(document, [ 'piscisMain' ]);
})();