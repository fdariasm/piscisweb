(function() {
	var app = angular.module('piscisConfig', [ 'ui.router', 'ngMessages',
			'ui.bootstrap', 'angularMoment', 'angular-growl' ]);
	var permisosProcesos = {};
	app.config([ "$stateProvider", "$urlRouterProvider",
			function($stateProvider, $urlRouterProvider) {
				$urlRouterProvider.otherwise("/main");
				$stateProvider.state('home', {
					url : "/main",
					templateUrl : '/vassets/views/config.html',
					controller : "configController"
				}).state('home.parametros', {
					url : '/parametros',
					controller : "parametrosController",
					templateUrl : '/vassets/views/config/parametros.html',
					proceso: 'parametros'
				}).state('home.usuarios', {
					url : '/usuarios',
					templateUrl : '/vassets/views/config/usuarios.html',
					controller : "usersController",
					proceso : 'usuarios'
				}).state('home.test', {
					url : '/test',
					template : '<h1>{{greeting}}</h1>',
					controller : "testController",
					proceso : 'test'
				}).state('home.roles', {
					url : '/roles',
					templateUrl : '/vassets/views/config/roles.html',
					controller : "rolesController",
					proceso : 'roles'
				}).state('home.modulos',{
					url : '/modulos',
					templateUrl : '/vassets/views/config/modulos.html',
					controller: "modulosController"
				});
			} ]);

	app.service('paramsService', getParametrosService());
	app.service('authService', getAuthService());
	app.service('usersService', UsersService);
	app.service('rolesService', RolesService);
	app.service('modulosService', modulosService);
	app.controller('ModalController', getModalController());
	app.controller('rolesController', getRolesController());
	app.controller('rolesUsuarioController', getRolesUsuarioController());
	app.controller('modulosController', getModulosController());
	app.controller("rolesModuloController", getRolesModuloController());

	app.run(function(amMoment) {
		amMoment.changeLocale('es');
	});

	app.directive('ngReallyClick', [ function() {
		return {
			restrict : 'A',
			link : function(scope, element, attrs) {
				element.bind('click', function() {
					var message = attrs.ngReallyMessage;
					if (message && confirm(message)) {
						scope.$apply(attrs.ngReallyClick);
					}
				});
			}
		};
	} ]);

	app.directive("canAccess", function(authService) {
		return {
			link : function($scope, $element, $attrs) {
				var proceso = $attrs.canAccess.trim();
				if (!authService.tienePermiso(proceso, "ingresar"))
					$element.css('display', 'none');
			}
		};
	});

	app.config([ 'growlProvider', function(growlProvider) {
		growlProvider.globalPosition('bottom-left');
		growlProvider.globalTimeToLive(5000);
	} ]);

	app.directive("canEdit", function(authService) {
		return {
			link : function($scope, $element, $attrs) {
				var proceso = $attrs.canEdit.trim();
				if (!authService.tienePermiso(proceso, "actualizar"))
					$element.css('display', 'none');
			}
		};
	});

	app.directive("canRemove", function(authService) {
		return {
			link : function($scope, $element, $attrs) {
				var proceso = $attrs.canRemove.trim();
				if (!authService.tienePermiso(proceso, "eliminar"))
					$element.css('display', 'none');
			}
		};
	});

	app.directive("canCreate", function(authService) {
		return {
			link : function($scope, $element, $attrs) {
				var proceso = $attrs.canCreate.trim();
				if (!authService.tienePermiso(proceso, "insertar"))
					$element.css('display', 'none');
			}
		};
	});

	app.directive("canUpdate", function(authService) {
		return {
			link : function($scope, $element, $attrs) {
				var proceso = $attrs.canInsert.trim();
				if (!authService.tienePermiso(proceso, "actualizar"))
					$element.css('display', 'none');
			}
		};
	});

	app.run([ 'authService', function(authService) {
		authService.setPermisos(permisosProcesos);
	} ]);

	app.run([
					'$rootScope',
					'$state',
					'authService',
					function($rootScope, $state, authService) {
						$rootScope
								.$on(
										"$stateChangeStart",
										function(event, toState, toParams,
												fromState, fromParams) {
											if (!('proceso' in toState))
												return;
											if (!authService
													.tienePermiso(
															toState.proceso,
															"ingresar")) {
												$rootScope.error = "Access undefined for this state";
												event.preventDefault();
												$state.go('home');
											}
										});
					} ]);

	angular.element(document).ready(function() {
		$.get('/api/auth/procesos/Configuraciones', function(data) {
			permisosProcesos = data;
			angular.bootstrap(document, [ 'piscisConfig' ]);
		});
	});
})();