package codegen

import scala.slick.model.Model
import scala.slick.{ model => m }
import scala.slick.codegen.SourceCodeGenerator


class CustomSourceCodeGen(model: m.Model) extends SourceCodeGenerator(model) {
  
  //customize Scala entity name (case class, etc.)
  //override def entityName = (dbName: String) => dbName.toCamelCase

  // override generator responsible for tables
  override def Table = new Table(_) {
    table =>

    /**
     * Indicates whether auto increment columns should be put last and made an Option with a None default.
     * Please set to !hlistEnabled for switching this on.
     * @group Basic customization overrides
     */
    override def autoIncLastAsOption = true

    override def EntityType = new EntityType {
      override def parents = Seq("models.domain.BaseEntity[Int]")
    }

    override def TableClass = new TableClass {
      override def parents = Seq("models.domain.TableWithId[Int]")
    }
  }
}

/** A runnable class to execute the code generator without further setup */
object CustomSourceCodeGenerator {
  import scala.slick.driver.JdbcProfile
  import scala.reflect.runtime.currentMirror
  def main(args: Array[String]) = {
    args.toList match {
      case slickDriver :: jdbcDriver :: url :: outputFolder :: pkg :: tail if tail.size == 0 || tail.size == 2 => {
        val driver: JdbcProfile = {
          val module = currentMirror.staticModule(slickDriver)
          val reflectedModule = currentMirror.reflectModule(module)
          val driver = reflectedModule.instance.asInstanceOf[JdbcProfile]
          driver
        }
        val db = driver.simple.Database
        (tail match {
          case user :: password :: Nil => db.forURL(url, driver = jdbcDriver, user = user, password = password)
          case Nil => db.forURL(url, driver = jdbcDriver)
          case _ => throw new Exception("This should never happen.")
        }).withSession { implicit session =>
          new CustomSourceCodeGen(driver.createModel()).writeToFile(slickDriver, outputFolder, pkg)
        }
      }
      case _ => {
        println("""
Usage:
  SourceCodeGenerator.main(Array(slickDriver, jdbcDriver, url, outputFolder, pkg))
  SourceCodeGenerator.main(Array(slickDriver, jdbcDriver, url, outputFolder, pkg, user, password))

slickDriver: Fully qualified name of Slick driver class, e.g. "scala.slick.driver.H2Driver"

jdbcDriver: Fully qualified name of jdbc driver class, e.g. "org.h2.Driver"

url: jdbc url, e.g. "jdbc:postgresql://localhost/test"

outputFolder: Place where the package folder structure should be put

pkg: Scala package the generated code should be places in

user: database connection user name

password: database connection password
            """.trim)
      }
    }
  }
}